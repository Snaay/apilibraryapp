﻿using AutoMapper;
using LibraryApp.Application.Common.NotificationMessage;
using LibraryApp.Application.Features.Authors.Commands.CreateAuthor;
using LibraryApp.Application.Features.Bookings.ViewModels;
using LibraryApp.Application.Features.Bookings.ViewModels.Book;
using LibraryApp.Application.Features.Books.ViewModels;
using LibraryApp.Application.Features.Books.ViewModels.BookModel;
using LibraryApp.Application.Features.Ganres.ViewModels;
using LibraryApp.Application.Features.Notifications.ViewModels;
using LibraryApp.Application.Features.Publishers.Commands.CreatePublisher;
using LibraryApp.Application.Features.Publishers.ViewModels;
using LibraryApp.Application.Features.Subganres.ViewModels;
using LibraryApp.Application.Features.User.ViewModals;
using LibraryApp.Application.Helpers;
using LibraryApp.Domain.Entities;
using LibraryApp.Domain.Entities.Notification;
using LibraryApp.Domain.Entities.User;
using LibraryApp.Domain.Enums;
using System.Linq;

namespace LibraryApp.Application.Mappings
{
    public class GeneralProfile : Profile
    {
        public GeneralProfile()
        {
            // Жанры
            CreateMap<Ganre, GanreViewModel>();
            CreateMap<Subganre, SubganreViewModel>();

            // Издатели
            CreateMap<CreatePublisherCommand, Publisher>();
            CreateMap<Publisher, PublisherViewModel>();

            //Авторы
            CreateMap<CreateAuthorCommand, Author>();

            //Брони
            CreateMap<Book, BookingBookViewModel>();
            CreateMap<Booking, BaseBookingViewModel>()
                .Include<Booking, BookingViewModal>()
                .Include<Booking, UserBookingViewModel>()
                .ForMember("BookingDate", options => options.MapFrom(d => HelperMethods.GetStringDate(d.BookingDate)))
                    .ForMember("BookingEndDate", options => options.MapFrom(d => HelperMethods.GetStringDate(d.BookingDate.AddMinutes(30))));
            CreateMap<Booking, BookingViewModal>();
            CreateMap<Booking, UserBookingViewModel>();

            //Пользователи
            CreateMap<ApplicationUser, UserListViewModel>()
                .ForMember("PhotoPath", options => options.MapFrom(a => a.UserProfile.PhotoPath))
                    .ForMember("FirstName", options => options.MapFrom(a => a.UserProfile.FirstName))
                        .ForMember("LastName", options => options.MapFrom(a => a.UserProfile.LastName))
                            .ForMember("MiddleName", options => options.MapFrom(a => a.UserProfile.MiddleName))
                                .ForMember("DateCreated", options => options.MapFrom(a => HelperMethods.GetStringDate(a.DateCreated)));

            //Книги
            CreateMap<GanreBook, SubganreViewModel>()
                .ForMember("Id", options => options.MapFrom(a => a.Subganre.Id))
                    .ForMember("Name", options => options.MapFrom(a => a.Subganre.Name));
            CreateMap<ApplicationUser, UserViewModel>()
                .ForMember("PhotoPath", options => options.MapFrom(a => a.UserProfile.PhotoPath))
                    .ForMember("FirstName", options => options.MapFrom(a => a.UserProfile.FirstName))
                        .ForMember("LastName", options => options.MapFrom(a => a.UserProfile.LastName))
                            .ForMember("MiddleName", options => options.MapFrom(a => a.UserProfile.MiddleName));
            CreateMap<Author, AuthorBookViewModel>();
            CreateMap<Publisher, PublisherBookViewModel>();
            CreateMap<Comment, CommentBookViewModel>()
                .ForMember("CommentDate", options => options.MapFrom(d => HelperMethods.GetStringDate(d.CommentDate)));

            CreateMap<Book, BookListViewModel>()
                .ForMember("LikesCount", options => options.MapFrom(a => a.Comments.Where(a => a.RatingTypeId == RatingType.Like).Count()))
                    .ForMember("DislikesCount", options => options.MapFrom(a => a.Comments.Where(a => a.RatingTypeId == RatingType.Dislike).Count()))
                        .ForMember("CommentCount", options => options.MapFrom(a => a.Comments.Count()));

            CreateMap<Book, BookDetailsViewModel>()
                .ForMember("Booking", options => options.MapFrom(a => a.Bookings.FirstOrDefault()))
                    .ForMember("Notify", options => options.MapFrom(a => a.SubNotifications.FirstOrDefault()))
                        .ForMember("LikesCount", options => options.MapFrom(a => a.Comments.Where(a => a.RatingTypeId == RatingType.Like).Count()))
                            .ForMember("DislikesCount", options => options.MapFrom(a => a.Comments.Where(a => a.RatingTypeId == RatingType.Dislike).Count()))
                                .ForMember("CommentCount", options => options.MapFrom(a => a.Comments.Count()));

            CreateMap<Booking, BookingViewModel>();
            CreateMap<SubNotification, SubNotificationViewModel>();
            // уведомления
            CreateMap<ReceiveNotification, NotificationViewModel>()
                .ForMember("Message", options => options.MapFrom(a => NotificationMessage.GetMessage(a.Notification.Book.Name)))
                    .ForMember("BookId", options => options.MapFrom(a => a.Notification.BookId))
                        .ForMember("ArrivalDate", options => options.MapFrom(a => HelperMethods.GetStringDate(a.Notification.ArrivalDate)));
        }
    }
}

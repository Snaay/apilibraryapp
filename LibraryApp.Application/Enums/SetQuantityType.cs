﻿namespace LibraryApp.Application.Enums
{
    public enum SetQuantityType : int
    {
        Inc = 0, //прибаить 1
        Dec = 1 //убавить 1
    }
}

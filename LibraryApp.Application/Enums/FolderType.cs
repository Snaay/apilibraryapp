﻿namespace LibraryApp.Application.Enums
{
    public enum FolderType : int
    {
        User = 0,
        Book = 1
    }
}

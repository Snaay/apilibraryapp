﻿namespace LibraryApp.Application.Enums
{
    public enum Roles
    {
        Admin,
        Librarian,
        Customer
    }
}

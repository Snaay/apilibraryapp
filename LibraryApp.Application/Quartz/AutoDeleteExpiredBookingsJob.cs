﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Features.Bookings.Commands.DeleteExpiredBooking;
using LibraryApp.Domain.Settings;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Quartz;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryApp.Application.Quartz
{
    [DisallowConcurrentExecution]
    public class AutoDeleteExpiredBookingsJob : IJob
    {
        private readonly IServiceProvider _provider;
        private readonly BookingSettings _bookingSettings;
        public AutoDeleteExpiredBookingsJob(IServiceProvider provider, IOptions<BookingSettings> bookingSettings)
        {
            _provider = provider;
            _bookingSettings = bookingSettings.Value;
        }
        public async Task Execute(IJobExecutionContext context)
        {
            using (var scop = _provider.CreateScope())
            {
                var _mediator = scop.ServiceProvider.GetService<IMediator>();

                var _context = scop.ServiceProvider.GetService<ILibraryDbContext>();

                var bookings = _context.Bookings
                                   .Where(a => a.IsGiven == false &&
                                       a.BookingDate.AddMinutes(_bookingSettings.LimitedBookingTimeMinutes) < DateTime.Now).ToList();

                if (bookings?.Count > 0)
                    foreach(var booking in bookings)
                        await _mediator.Publish(new DeleteExpiredBookingCommand { Booking = booking });
            }

            await Task.CompletedTask;
        }
    }
}

﻿namespace LibraryApp.Application.DTO.File
{
    public class DeleteFileRequest
    {
        public string FilePath { get; set; }
    }
}

﻿using Microsoft.AspNetCore.Http;
using System.IO;

namespace LibraryApp.Application.DTO.File
{
    public class UpdateFileRequest
    {
        public string FilePath { get; set; }
        public IFormFile FileStream { get; set; }
    }
}

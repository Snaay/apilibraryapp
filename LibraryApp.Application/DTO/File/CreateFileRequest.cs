﻿using LibraryApp.Application.Enums;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace LibraryApp.Application.DTO.File
{
    public class CreateFileRequest
    {
        public FolderType FolderTypeId { get; set; }
        public IFormFile FileStream { get; set; }
    }
}

﻿using System.Text.Json.Serialization;

namespace LibraryApp.Application.DTO.Auth
{
    public class AuthenticationResponse
    {
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string PhotoPath { get; set; }
        public string Email { get; set; }
        public string[] Roles { get; set; }
        [JsonIgnore]
        public string Token { get; set; }
        [JsonIgnore]
        public string RefreshToken { get; set; }
    }
}

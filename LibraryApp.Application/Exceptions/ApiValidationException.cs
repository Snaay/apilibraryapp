﻿using FluentValidation.Results;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace LibraryApp.Application.Exceptions
{
    public class ApiValidationException : Exception
    {
        public ApiValidationException() : base("Произошла одна или несколько ошибок проверки.")
        {
            Errors = new List<string>();
        }
        public ApiValidationException(IEnumerable<ValidationFailure> failures) : this()
        {
            foreach(var failure in failures)
            {
                Errors.Add(failure.ErrorMessage);
            }
        }
        public ApiValidationException(IEnumerable<IdentityError> errors) : this()
        {
            foreach (var error in errors)
            {
                Errors.Add(error.Description);
            }
        }
        public List<string> Errors { get; }
    }
}

﻿using System.Collections.Generic;

namespace LibraryApp.Application.Wrappers
{
    public class ListResponse<T> where T : class
    {
        public IEnumerable<T> Items { get; set; }
        public int TotalItemsCount { get; set; }
    }
}

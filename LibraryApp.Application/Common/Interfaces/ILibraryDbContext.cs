﻿using LibraryApp.Domain.Entities;
using LibraryApp.Domain.Entities.Notification;
using LibraryApp.Domain.Entities.User;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Common.Interfaces
{
    public interface ILibraryDbContext : IDisposable
    {
        DbSet<ApplicationUser> Users { get; set; }
        DbSet<UserProfile> UserProfiles { get; set; }
        DbSet<Book> Books { get; set; }
        DbSet<Booking> Bookings { get; set; }
        DbSet<Author> Authors { get; set; }
        DbSet<Publisher> Publishers { get; set; }
        DbSet<Ganre> Ganres { get; set; }
        DbSet<Subganre> Subganres { get; set; }
        DbSet<Notification> Notifications { get; set; }
        DbSet<ReceiveNotification> ReceiveNotifications { get; set; }
        DbSet<SubNotification> SubNotifications { get; set; }
        DbSet<Comment> Comments { get; set; }
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}

﻿using LibraryApp.Application.DTO.Email;
using System.Threading.Tasks;

namespace LibraryApp.Application.Common.Interfaces
{
    public interface IEmailService
    {
        Task SendAsync(EmailRequest request);
    }
}

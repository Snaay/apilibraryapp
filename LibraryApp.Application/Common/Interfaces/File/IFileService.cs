﻿using LibraryApp.Application.DTO.File;
using System.Threading.Tasks;

namespace LibraryApp.Application.Common.Interfaces.File
{
    public interface IFileService 
    {
        Task<string> CreateFile(CreateFileRequest request);
        Task<string> UpdateFile(UpdateFileRequest request);
        Task DeleteFile(DeleteFileRequest request);
    }
}

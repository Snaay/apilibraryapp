﻿namespace LibraryApp.Application.Common.NotificationMessage
{
    public static class NotificationMessage
    {
        public static string GetMessage(string bookName)
        {
            return string.Format($"Книга \"{bookName}\" доступна для бронирования");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace LibraryApp.Application.Helpers
{
    public static class HelperMethods
    {
        public static string GetStringDate(DateTime date)
        {
            return string.Format("{0} {1}г. {2}", date.ToString("m"), date.Year, date.ToString("t"));
        }

        public static int GetSkipStartPagination(int currentPage, int pageSize)
        {
            return (currentPage - 1) * pageSize;
        }

        public static string[] SplitSearchString(string search)
        {
            string[] result = search?.ToLower().Split(' ')
                                .Where(a => !string.IsNullOrEmpty(a))
                                    .ToArray();

            return result?.Length > 0 ? result : null;
        }

        public static string[] GetRolesFromClaims(List<Claim> claims)
        {
            var roles = new List<string>();

            claims.FindAll(a => a.Type == "roles")
                .ForEach(a => roles.Add(a.Value));

            return roles.ToArray();
        }
    }
}

﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Features.Publishers.ViewModels;
using LibraryApp.Application.Helpers;
using LibraryApp.Application.Wrappers;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.Publishers.Queries.GetPublishers
{
    public class GetPublishersQuery : IRequest<ListResponse<PublisherViewModel>> 
    {
        public string Search { get; set; }
        public int CurrentPage { get; set; } = 1;
        public int PageSize { get; set; } = 20;
    }
    public class GetPublishersQueryHandler : IRequestHandler<GetPublishersQuery, ListResponse<PublisherViewModel>>
    {
        private readonly ILibraryDbContext _context;
        public GetPublishersQueryHandler(ILibraryDbContext context)
        {
            _context = context;
        }
        public async Task<ListResponse<PublisherViewModel>> Handle(GetPublishersQuery request, CancellationToken cancellationToken)
        {
            var response = new ListResponse<PublisherViewModel>();

            int skipStart = HelperMethods.GetSkipStartPagination(request.CurrentPage, request.PageSize);

            var searchItems = HelperMethods.SplitSearchString(request.Search);

            var publishers = _context.Publishers.Select(a => new PublisherViewModel
            {
                Id = a.Id,
                Name = a.Name,
                CountBooks = a.Books.Count
            }).AsNoTracking();

            if (searchItems != null)
                foreach (var search in searchItems)
                    publishers = publishers.Where(a => a.Name.ToLower().Contains(search));

            response.TotalItemsCount = await publishers.CountAsync(cancellationToken);

            response.Items = publishers.Skip(skipStart).Take(request.PageSize);

            return response;
        }
    }
}

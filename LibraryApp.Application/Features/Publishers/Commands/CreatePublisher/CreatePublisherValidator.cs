﻿using FluentValidation;

namespace LibraryApp.Application.Features.Publishers.Commands.CreatePublisher
{
    public class CreatePublisherValidator : AbstractValidator<CreatePublisherCommand>
    {
        public CreatePublisherValidator()
        {
            RuleFor(a => a.Name).NotEmpty().WithMessage("Введите имя издатель");
        }
    }
}

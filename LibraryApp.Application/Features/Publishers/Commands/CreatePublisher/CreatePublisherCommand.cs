﻿using AutoMapper;
using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Wrappers;
using LibraryApp.Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.Publishers.Commands.CreatePublisher
{
    public class CreatePublisherCommand : IRequest<Response<int>>
    {
        public string Name { get; set; }
    }
    public class CreatePublisherCommandHandler : IRequestHandler<CreatePublisherCommand, Response<int>>
    {
        private readonly ILibraryDbContext _context;
        private readonly IMapper _mapper;
        public CreatePublisherCommandHandler(ILibraryDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<Response<int>> Handle(CreatePublisherCommand request, CancellationToken cancellationToken)
        {
            var publisher = await _context.Publishers.AddAsync(_mapper.Map<Publisher>(request));

            await _context.SaveChangesAsync(cancellationToken);

            return new Response<int>(publisher.Entity.Id, "Издатель добавлен");
        }
    }
}

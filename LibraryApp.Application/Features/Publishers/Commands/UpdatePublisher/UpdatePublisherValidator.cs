﻿using FluentValidation;

namespace LibraryApp.Application.Features.Publishers.Commands.UpdatePublisher
{
    public class UpdatePublisherValidator : AbstractValidator<UpdatePublisherCommand>
    {
        public UpdatePublisherValidator()
        {
            RuleFor(a => a.PublisherId).NotEmpty().WithMessage("Id издателя пустой");
            RuleFor(a => a.Name).NotEmpty().WithMessage("Введите имя издатель");
        }
    }
}

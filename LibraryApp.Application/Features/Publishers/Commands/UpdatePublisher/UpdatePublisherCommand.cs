﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Exceptions;
using LibraryApp.Application.Wrappers;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.Publishers.Commands.UpdatePublisher
{
    public class UpdatePublisherCommand : IRequest<Response<int>>
    {
        public int PublisherId { get; set; }
        public string Name { get; set; }
    }
    public class UpdatePublisherCommandHandler : IRequestHandler<UpdatePublisherCommand, Response<int>>
    {
        private readonly ILibraryDbContext _context;
        public UpdatePublisherCommandHandler(ILibraryDbContext context) => _context = context;
        public async Task<Response<int>> Handle(UpdatePublisherCommand request, CancellationToken cancellationToken)
        {
            var publisher = await _context.Publishers.FirstOrDefaultAsync(a => a.Id == request.PublisherId)
                ?? throw new ApiException("Издатель не найден");

            publisher.Name = request.Name;

            await _context.SaveChangesAsync(cancellationToken);

            return new Response<int>(publisher.Id, "Издатель удален");
        }
    }
}

﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Exceptions;
using LibraryApp.Application.Wrappers;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.Publishers.Commands.DeletePublisher
{
    public class DeletePublisherCommand : IRequest<Response<int>>
    {
        public int PublisherId { get; set; }
    }
    public class DeletePublisherCommandHandler : IRequestHandler<DeletePublisherCommand, Response<int>>
    {
        private readonly ILibraryDbContext _context;
        public DeletePublisherCommandHandler(ILibraryDbContext context) => _context = context;
        public async Task<Response<int>> Handle(DeletePublisherCommand request, CancellationToken cancellationToken)
        {
            var publisher = await _context.Publishers.FirstOrDefaultAsync(a => a.Id == request.PublisherId) 
                ?? throw new ApiException("Издатель не найден");

            _context.Publishers.Remove(publisher);

            await _context.SaveChangesAsync(cancellationToken);

            return new Response<int>(publisher.Id, "Издатель удален");
        }
    }
}

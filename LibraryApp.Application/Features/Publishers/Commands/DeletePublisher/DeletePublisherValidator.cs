﻿using FluentValidation;

namespace LibraryApp.Application.Features.Publishers.Commands.DeletePublisher
{
    public class DeletePublisherValidator : AbstractValidator<DeletePublisherCommand>
    {
        public DeletePublisherValidator()
        {
            RuleFor(a => a.PublisherId).NotEmpty().WithMessage("Id издателя пустой");
        }
    }
}

﻿namespace LibraryApp.Application.Features.Publishers.ViewModels
{
    public class PublisherViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CountBooks { get; set; }
    }
}

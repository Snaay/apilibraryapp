﻿using FluentValidation;
using LibraryApp.Application.Features.Validators;

namespace LibraryApp.Application.Features.Auth.Commands.ResetPassword
{
    public class ResetPasswordValidation : AbstractValidator<ResetPasswordCommand>
    {
        public ResetPasswordValidation()
        {
            RuleFor(x => x.Email)
                .NotEmpty().WithMessage("Email отсутствует");
            RuleFor(x => x.Token)
                .NotEmpty().WithMessage("Token отсутствует");
            RuleFor(x => x.Password)
                .Password()
                .Equal(a => a.ConfirmPassword).WithMessage("Пароли не совпадают");
        }
    }
}

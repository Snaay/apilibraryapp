﻿using LibraryApp.Application.Exceptions;
using LibraryApp.Application.Wrappers;
using LibraryApp.Domain.Entities.User;
using MediatR;
using Microsoft.AspNetCore.Identity;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace LibraryApp.Application.Features.Auth.Commands.ResetPassword
{
    public class ResetPasswordCommand : IRequest<Response<string>>
    {
        public string Email { get; set; }
        public string Token { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }
    public class ResetPasswordCommandHandler : IRequestHandler<ResetPasswordCommand, Response<string>>
    {
        private readonly UserManager<ApplicationUser> _userManager;
        public ResetPasswordCommandHandler(UserManager<ApplicationUser> userManager) => _userManager = userManager;
        public async Task<Response<string>> Handle(ResetPasswordCommand request, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByEmailAsync(request.Email);

            if (user == null)
                return new Response<string>("Пользователь не найден.");

            request.Token = HttpUtility.UrlDecode(request.Token);

            var result = await _userManager.ResetPasswordAsync(user, request.Token, request.Password);

            if (!result.Succeeded)
                throw new ApiException("Произошла ошибка при сбросе пароля.");

            return new Response<string>(user.Email, "Пароль сброшен.");
        }
    }
}

﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.DTO.Auth;
using LibraryApp.Application.Exceptions;
using LibraryApp.Application.Wrappers;
using LibraryApp.Domain.Entities.User;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.Auth.Commands.AuthMe
{
    public class AuthMeCommand : IRequest<Response<AuthenticationResponse>> { }
    public class AuthMeCommandHandler : IRequestHandler<AuthMeCommand, Response<AuthenticationResponse>>
    {
        private readonly ILibraryDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ICurrentUserService _currentUserService;
        public AuthMeCommandHandler(ILibraryDbContext context, UserManager<ApplicationUser> userManager, ICurrentUserService currentUserService)
        {
            _context = context;
            _userManager = userManager;
            _currentUserService = currentUserService;
        }
        public async Task<Response<AuthenticationResponse>> Handle(AuthMeCommand request, CancellationToken cancellationToken)
        {
            var user = await _context.Users
                                .AsNoTracking()
                                    .Include(a => a.UserProfile)
                                        .FirstOrDefaultAsync(a => a.Id == _currentUserService.UserId)
                                            ?? throw new ApiException("Не авторизованы");

            var roles = await _userManager.GetRolesAsync(user);

            return new Response<AuthenticationResponse>(new AuthenticationResponse
            {
                UserName = user.UserName,
                FirstName = user.UserProfile?.FirstName,
                LastName = user.UserProfile?.LastName,
                MiddleName = user.UserProfile?.MiddleName,
                PhotoPath = user.UserProfile?.PhotoPath,
                Email = user.Email,
                Roles = roles.ToArray()
            }, "Авторизован");
        }
    }
}

﻿using FluentValidation;

namespace LibraryApp.Application.Features.Auth.Commands.ConfirmEmail
{
    public class ConfirmEmailValidator : AbstractValidator<ConfirmEmailCommand>
    {
        public ConfirmEmailValidator()
        {
            RuleFor(x => x.UserId)
                .NotEmpty().WithMessage("Id пользователя отсутствует");
            RuleFor(x => x.Token)
                .NotEmpty().WithMessage("Token отсутствует");
        }
    }
}

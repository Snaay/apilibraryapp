﻿using FluentValidation;
using LibraryApp.Application.Features.Validators;

namespace LibraryApp.Application.Features.Auth.Commands.Login
{
    public class LoginValidator : AbstractValidator<LoginCommand>
    {
        public LoginValidator()
        {
            RuleFor(a => a.UserName)
                .Login();
            RuleFor(x => x.Password)
                .NotEmpty().WithMessage("Пароль обязательный для заполнения");
        }
    }
}

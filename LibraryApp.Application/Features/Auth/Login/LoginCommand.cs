﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.DTO.Auth;
using LibraryApp.Application.Exceptions;
using LibraryApp.Application.Wrappers;
using LibraryApp.Domain.Entities.User;
using LibraryApp.Domain.Settings;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using LibraryApp.Application.Helpers;

namespace LibraryApp.Application.Features.Auth.Commands.Login
{
    public class LoginCommand : IRequest<Response<AuthenticationResponse>>
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
    public class LoginCommandHandler : IRequestHandler<LoginCommand, Response<AuthenticationResponse>>
    {
        private readonly ILibraryDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly JwtSettings _jwtSettings;
        public LoginCommandHandler(ILibraryDbContext context, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IOptions<JwtSettings> jwtSettings)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
            _jwtSettings = jwtSettings.Value;
        }
        public async Task<Response<AuthenticationResponse>> Handle(LoginCommand request, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByNameAsync(request.UserName)
                            ?? await _userManager.FindByEmailAsync(request.UserName);

            if (user == null)
                throw new ApiException($"Пользователь с таким логином '{request.UserName}' не найден.");

            var result = await _signInManager.CheckPasswordSignInAsync(user, request.Password, false);

            if (!result.Succeeded)
                throw new ApiException("Неправильный логин и (или) пароль.");

            if (!user.EmailConfirmed)
                throw new ApiException($"Аккаунт не подтвержден для {user.Email}.");

            var profile = await _context.UserProfiles.FirstOrDefaultAsync(a => a.UserId == user.Id); // не правильно, переделать!

            var claims = await CreateUserClaims(user);

            var token = CreateToken(claims);

            var response = new AuthenticationResponse
            {
                UserName = user.UserName,
                FirstName = profile?.FirstName,
                LastName = profile?.LastName,
                MiddleName = profile?.MiddleName,
                PhotoPath = profile?.PhotoPath,
                Email = user.Email,
                Roles = HelperMethods.GetRolesFromClaims(claims),
                Token = new JwtSecurityTokenHandler().WriteToken(token),
                RefreshToken = null // сделать
            };

            return new Response<AuthenticationResponse>(response, $"Аутентифицирован {user.UserName}");
        }

        private async Task<List<Claim>> CreateUserClaims(ApplicationUser user)
        {
            var roles = await _userManager.GetRolesAsync(user);

            var roleClaims = new List<Claim>();

            foreach (var role in roles)
                roleClaims.Add(new Claim("roles", role));

            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                new Claim("uid", user.Id)
            }.Union(roleClaims).ToList();

            return claims;
        }
        private JwtSecurityToken CreateToken(List<Claim> claims)
        {
            var symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtSettings.Key));

            var signingCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                issuer: _jwtSettings.Issuer,
                audience: _jwtSettings.Audience,
                claims: claims,
                expires: DateTime.UtcNow.AddMinutes(_jwtSettings.LifeTime),
                signingCredentials: signingCredentials);

            return token;
        }
    }
}

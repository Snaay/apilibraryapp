﻿using FluentValidation;
using LibraryApp.Application.Features.Validators;

namespace LibraryApp.Application.Features.Auth.Commands.ForgotPassword
{
    public class ForgotPasswordValidator : AbstractValidator<ForgotPasswordCommand>
    {
        public ForgotPasswordValidator()
        {
            RuleFor(x => x.Email)
                .Email();
        }
    }
}

﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.DTO.Email;
using LibraryApp.Application.Exceptions;
using LibraryApp.Application.Wrappers;
using LibraryApp.Domain.Entities.User;
using LibraryApp.Domain.Settings;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Options;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace LibraryApp.Application.Features.Auth.Commands.ForgotPassword
{
    public class ForgotPasswordCommand : IRequest<Response<string>>
    {
        public string Email { get; set; }
        public string Origin { get; set; }
    }
    public class ForgotPasswordCommandHandler : IRequestHandler<ForgotPasswordCommand, Response<string>>
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IEmailService _emailService;
        private readonly MailSettings _mailSettings;
        public ForgotPasswordCommandHandler(UserManager<ApplicationUser> userManager, IEmailService emailService, IOptions<MailSettings> mailSettings)
        {
            _userManager = userManager;
            _emailService = emailService;
            _mailSettings = mailSettings.Value;
        }
        public async Task<Response<string>> Handle(ForgotPasswordCommand request, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByEmailAsync(request.Email);

            if (user == null)
                throw new ApiException("Пользователь не найден.");

            var token = HttpUtility.UrlEncode(await _userManager.GeneratePasswordResetTokenAsync(user));

            string route = "reset-password";

            var uri = new Uri(string.Concat($"{request.Origin}/", route));

            var verificationUri = QueryHelpers.AddQueryString(uri.ToString(), "email", user.Email);

            verificationUri = QueryHelpers.AddQueryString(verificationUri, "token", token);

            var emailReuest = new EmailRequest
            {
                From = _mailSettings.EmailFrom,
                To = user.Email,
                Subject = "Сброс пароля",
                Body = $"Перейдя по ссылке: {verificationUri}"
            };

            await _emailService.SendAsync(emailReuest);

            return new Response<string>(user.Email, "Сообщение по сбросу пароля отправлено на почту.");
        }
    }
}

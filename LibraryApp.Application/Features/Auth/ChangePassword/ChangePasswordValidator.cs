﻿using FluentValidation;
using LibraryApp.Application.Features.Validators;

namespace LibraryApp.Application.Features.Auth.Commands.ChangePassword
{
    public class ChangePasswordValidator : AbstractValidator<ChangePasswordCommand>
    {
        public ChangePasswordValidator()
        {
            RuleFor(x => x.OldPassword).NotEmpty().WithMessage("Старый пароль обязательный для заполнения");
            RuleFor(x => x.NewPassword).Password()
                .Equal(a => a.ConfirmNewPassword)
                .WithMessage("Пароли не совпадают");
        }
    }
}

﻿using FluentValidation;
using LibraryApp.Application.Features.Validators;

namespace LibraryApp.Application.Features.Auth.Commands.Register
{
    public class RegisterCommandValidation : AbstractValidator<RegisterCommand>
    {
        public RegisterCommandValidation()
        {
            RuleFor(x => x.Email)
                .Email();
            RuleFor(x => x.UserName)
                .Login();
            RuleFor(x => x.Password)
                .Password()
                .Equal(a => a.ConfirmPassword)
                .WithMessage("Пароли не совпадают");
        }
    }
}

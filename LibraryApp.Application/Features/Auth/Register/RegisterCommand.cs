﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.DTO.Email;
using LibraryApp.Application.Enums;
using LibraryApp.Application.Exceptions;
using LibraryApp.Application.Wrappers;
using LibraryApp.Domain.Entities.User;
using LibraryApp.Domain.Settings;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Options;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace LibraryApp.Application.Features.Auth.Commands.Register
{
    public class RegisterCommand : IRequest<Response<string>>
    {
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }

        public string Origin { get; set; }
    }
    public class RegisterCommandHandler : IRequestHandler<RegisterCommand, Response<string>>
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILibraryDbContext _context;
        private readonly IEmailService _emailService;
        private readonly MailSettings _mailSettings;
        public RegisterCommandHandler(UserManager<ApplicationUser> userManager, ILibraryDbContext context, IEmailService emailService, IOptions<MailSettings> mailSettings)
        {
            _userManager = userManager;
            _context = context;
            _emailService = emailService;
            _mailSettings = mailSettings.Value;
        }
        public async Task<Response<string>> Handle(RegisterCommand request, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByNameAsync(request.UserName)
                            ?? await _userManager.FindByEmailAsync(request.Email);
            if (user != null)
                throw new ApiException(string.Format("Пользователь с таким {0} уже существует.", (request.UserName == user.UserName) ? "логином" : "email-ом"));

            user = new ApplicationUser
            {
                UserName = request.UserName,
                Email = request.Email,
                DateCreated = DateTime.UtcNow
            };

            var result = await _userManager.CreateAsync(user, request.Password);

            if (!result.Succeeded)
                throw new ApiValidationException(result.Errors);

            await _userManager.AddToRoleAsync(user, Roles.Customer.ToString());

            var profile = new UserProfile
            {
                FirstName = null,
                LastName = null,
                MiddleName = null,
                PhotoPath = null,
                UserId = user.Id
            };

            await _context.UserProfiles.AddAsync(profile);

            await _context.SaveChangesAsync(cancellationToken);

            var token = HttpUtility.UrlEncode(await _userManager.GenerateEmailConfirmationTokenAsync(user));

            string route = "confirm-email";

            var uri = new Uri(string.Concat($"{request.Origin}/", route));

            var verificationUri = QueryHelpers.AddQueryString(uri.ToString(), "userId", user.Id);

            verificationUri = QueryHelpers.AddQueryString(verificationUri, "token", token);

            var emailReuest = new EmailRequest
            {
                From = _mailSettings.EmailFrom,
                To = user.Email,
                Subject = "Подтвердить регистрацию",
                Body = $"Пожалуйста, подтвердите свою учетную запись, посетив этот URL: {verificationUri}"
            };

            await _emailService.SendAsync(emailReuest);

            return new Response<string>(user.Email, $"Пользователь зарегистрирован. Пожалуйста, подтвердите свой аккаунт, перейдя по ссылки отправленной вам на почту.");
        }
    }
}

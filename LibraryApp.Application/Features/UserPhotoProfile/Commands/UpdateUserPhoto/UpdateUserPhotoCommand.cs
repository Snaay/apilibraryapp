﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Common.Interfaces.File;
using LibraryApp.Application.DTO.File;
using LibraryApp.Application.Exceptions;
using LibraryApp.Application.Wrappers;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.UserPhotoProfile.Commands.UpdateUserPhoto
{
    public class UpdateUserPhotoCommand : IRequest<Response<string>>
    {
        public IFormFile Photo { get; set; }
    }
    public class UpdateUserPhotoCommandHandler : IRequestHandler<UpdateUserPhotoCommand, Response<string>>
    {
        private readonly ILibraryDbContext _context;
        private readonly ICurrentUserService _currentUserService;
        private readonly IFileService _fileService;
        public UpdateUserPhotoCommandHandler(ILibraryDbContext context, ICurrentUserService currentUserService, IFileService fileService)
        {
            _context = context;
            _currentUserService = currentUserService;
            _fileService = fileService;
        }
        public async Task<Response<string>> Handle(UpdateUserPhotoCommand request, CancellationToken cancellationToken)
        {
            var userProfile = await _context.UserProfiles
                .FirstOrDefaultAsync(a => a.UserId == _currentUserService.UserId)
                    ?? throw new ApiException("Профиль не найден");

            if (string.IsNullOrEmpty(userProfile.PhotoPath))
                throw new ApiException("Фото профиля не найдено");

            var fileRequest = new UpdateFileRequest()
            {
                FilePath = userProfile.PhotoPath,
                FileStream = request.Photo
            };

            var filePathResult = await _fileService.UpdateFile(fileRequest);

            userProfile.PhotoPath = filePathResult;

            await _context.SaveChangesAsync(cancellationToken);

            return new Response<string>(filePathResult, "Фото обновлено");
        }
    }
}

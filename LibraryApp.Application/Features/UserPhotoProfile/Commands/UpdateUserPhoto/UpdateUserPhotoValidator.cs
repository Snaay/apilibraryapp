﻿using FluentValidation;
using LibraryApp.Application.Features.Validators;

namespace LibraryApp.Application.Features.UserPhotoProfile.Commands.UpdateUserPhoto
{
    public class UpdateUserPhotoValidator : AbstractValidator<UpdateUserPhotoCommand>
    {
        public UpdateUserPhotoValidator()
        {
            RuleFor(a => a.Photo).Photo();
        }
    }
}

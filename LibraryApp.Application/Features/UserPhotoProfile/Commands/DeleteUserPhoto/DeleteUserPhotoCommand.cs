﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Common.Interfaces.File;
using LibraryApp.Application.DTO.File;
using LibraryApp.Application.Exceptions;
using LibraryApp.Application.Wrappers;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.UserPhotoProfile.Commands.DeleteUserPhoto
{
    public class DeleteUserPhotoCommand : IRequest<Response<string>> { }
    public class DeleteUserPhotoCommandHandler : IRequestHandler<DeleteUserPhotoCommand, Response<string>>
    {
        private readonly ILibraryDbContext _context;
        private readonly ICurrentUserService _currentUserService;
        private readonly IFileService _fileService;
        public DeleteUserPhotoCommandHandler(ILibraryDbContext context, ICurrentUserService currentUserService, IFileService fileService)
        {
            _context = context;
            _currentUserService = currentUserService;
            _fileService = fileService;
        }
        public async Task<Response<string>> Handle(DeleteUserPhotoCommand request, CancellationToken cancellationToken)
        {
            var userProfile = await _context.UserProfiles
                .FirstOrDefaultAsync(a => a.UserId == _currentUserService.UserId)
                    ?? throw new ApiException("Профиль не найден");

            var deleteFileRequest = new DeleteFileRequest
            {
                FilePath = userProfile.PhotoPath
            };

            await _fileService.DeleteFile(deleteFileRequest);

            userProfile.PhotoPath = null;

            await _context.SaveChangesAsync(cancellationToken);

            return new Response<string>(null, "Фото удалено");
        }
    }
}

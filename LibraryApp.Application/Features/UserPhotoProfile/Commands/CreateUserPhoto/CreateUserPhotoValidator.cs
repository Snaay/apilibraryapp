﻿using FluentValidation;
using LibraryApp.Application.Features.Validators;

namespace LibraryApp.Application.Features.UserPhotoProfile.Commands.CreateUserPhoto
{
    public class CreateUserPhotoValidator : AbstractValidator<CreateUserPhotoCommand>
    {
        public CreateUserPhotoValidator()
        {
            RuleFor(f => f.Photo).Photo();
        }
    }
}

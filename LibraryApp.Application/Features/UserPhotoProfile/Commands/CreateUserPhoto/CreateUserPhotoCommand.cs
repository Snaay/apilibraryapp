﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Common.Interfaces.File;
using LibraryApp.Application.DTO.File;
using LibraryApp.Application.Enums;
using LibraryApp.Application.Exceptions;
using LibraryApp.Application.Wrappers;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.UserPhotoProfile.Commands.CreateUserPhoto
{
    public class CreateUserPhotoCommand : IRequest<Response<string>>
    {
        public IFormFile Photo { get; set; }
    }
    public class CreateUserPhotoCommandHandler : IRequestHandler<CreateUserPhotoCommand, Response<string>>
    {
        private readonly ILibraryDbContext _context;
        private readonly ICurrentUserService _currentUserService;
        private readonly IFileService _fileService;
        public CreateUserPhotoCommandHandler(ILibraryDbContext context, ICurrentUserService currentUserService, IFileService fileService)
        {
            _context = context;
            _currentUserService = currentUserService;
            _fileService = fileService;
        }
        public async Task<Response<string>> Handle(CreateUserPhotoCommand request, CancellationToken cancellationToken)
        {
            var userProfile = await _context.UserProfiles
                .FirstOrDefaultAsync(a => a.UserId == _currentUserService.UserId)
                    ?? throw new ApiException("Профиль не найден");

            if (!string.IsNullOrEmpty(userProfile.PhotoPath))
                throw new ApiException("Вы уже добавляли фото профиля");

            var fileRequest = new CreateFileRequest
            {
                FileStream = request.Photo,
                FolderTypeId = FolderType.User
            };

            var filePathResult = await _fileService.CreateFile(fileRequest);

            userProfile.PhotoPath = filePathResult;

            await _context.SaveChangesAsync(cancellationToken);

            return new Response<string>(filePathResult, "Фото добавлено");
        }
    }
}

﻿namespace LibraryApp.Application.Features.Subganres.ViewModels
{
    public class SubganreViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}

﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Exceptions;
using LibraryApp.Application.Wrappers;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.Subganres.Commands.DeleteSubganre
{
    public class DeleteSubganreCommand : IRequest<Response<int>>
    {
        public int SubganreId { get; set; }
    }
    public class DeleteSubganreCommandHandler : IRequestHandler<DeleteSubganreCommand, Response<int>>
    {
        private readonly ILibraryDbContext _context;
        public DeleteSubganreCommandHandler(ILibraryDbContext context) => _context = context;
        public async Task<Response<int>> Handle(DeleteSubganreCommand request, CancellationToken cancellationToken)
        {
            var subganre = await _context.Subganres.FirstOrDefaultAsync(a => a.Id == request.SubganreId)
                ?? throw new ApiException("Поджанр не найден");

            _context.Subganres.Remove(subganre);

            await _context.SaveChangesAsync(cancellationToken);

            return new Response<int>(subganre.Id, "Поджанр удален");
        }
    }
}

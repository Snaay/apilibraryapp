﻿using FluentValidation;

namespace LibraryApp.Application.Features.Subganres.Commands.DeleteSubganre
{
    public class DeleteSubganreValidator : AbstractValidator<DeleteSubganreCommand>
    {
        public DeleteSubganreValidator()
        {
            RuleFor(a => a.SubganreId).NotEmpty().WithMessage("Id поджанра отсутствует");
        }
    }
}

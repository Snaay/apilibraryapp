﻿using FluentValidation;

namespace LibraryApp.Application.Features.Subganres.Commands.UpdateSubganre
{
    public class UpdateSubganreValidator : AbstractValidator<UpdateSubganreCommand>
    {
        public UpdateSubganreValidator()
        {
            RuleFor(a => a.SubganreId).NotEmpty().WithMessage("Id поджанра отсутствует");
            RuleFor(a => a.Name).NotEmpty().WithMessage("Имя поджанра обязательно для заполнения");
        }
    }
}

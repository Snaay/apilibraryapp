﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Exceptions;
using LibraryApp.Application.Wrappers;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.Subganres.Commands.UpdateSubganre
{
    public class UpdateSubganreCommand : IRequest<Response<int>>
    {
        public int SubganreId { get; set; }
        public string Name { get; set; }
    }
    public class UpdateSubganreCommandHandler : IRequestHandler<UpdateSubganreCommand, Response<int>>
    {
        private readonly ILibraryDbContext _context;
        public UpdateSubganreCommandHandler(ILibraryDbContext context) => _context = context;
        public async Task<Response<int>> Handle(UpdateSubganreCommand request, CancellationToken cancellationToken)
        {
            var subganre = await _context.Subganres.FirstOrDefaultAsync(a => a.Id == request.SubganreId)
                ?? throw new ApiException("Жанр не найден");

            subganre.Name = request.Name;

            await _context.SaveChangesAsync(cancellationToken);

            return new Response<int>(subganre.Id, "Поджанр обновлен");
        }
    }
}

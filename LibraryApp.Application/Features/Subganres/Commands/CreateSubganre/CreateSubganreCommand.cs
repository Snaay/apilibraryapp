﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Exceptions;
using LibraryApp.Application.Wrappers;
using LibraryApp.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.Subganres.Commands.CreateSubganre
{
    public class CreateSubganreCommand : IRequest<Response<int>>
    {
        public int GanreId { get; set; }
        public string Name { get; set; }
    }
    public class CreateSubganreCommandHandler : IRequestHandler<CreateSubganreCommand, Response<int>>
    {
        private readonly ILibraryDbContext _context;
        public CreateSubganreCommandHandler(ILibraryDbContext context) => _context = context;
        public async Task<Response<int>> Handle(CreateSubganreCommand request, CancellationToken cancellationToken)
        {
            var ganre = await _context.Ganres.FirstOrDefaultAsync(a => a.Id == request.GanreId)
                ?? throw new ApiException("Жанр не найден");

            var subganre = await _context.Subganres.AddAsync(new Subganre { Name = request.Name, GanreId = ganre.Id });

            await _context.SaveChangesAsync(cancellationToken);

            return new Response<int>(subganre.Entity.Id, "Поджанр добавлен");
        }
    }
}

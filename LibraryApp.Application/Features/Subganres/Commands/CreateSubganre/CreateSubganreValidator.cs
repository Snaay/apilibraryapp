﻿using FluentValidation;

namespace LibraryApp.Application.Features.Subganres.Commands.CreateSubganre
{
    public class CreateSubganreValidator : AbstractValidator<CreateSubganreCommand>
    {
        public CreateSubganreValidator()
        {
            RuleFor(a => a.GanreId).NotEmpty().WithMessage("Id жанра отсутствует");
            RuleFor(a => a.Name).NotEmpty().WithMessage("Имя поджанра обязательно для заполнения");
        }
    }
}

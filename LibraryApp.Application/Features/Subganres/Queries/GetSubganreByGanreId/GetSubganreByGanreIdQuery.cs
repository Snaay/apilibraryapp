﻿using AutoMapper;
using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Features.Subganres.ViewModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.Subganres.Queries.GetSubganreByGanreId
{
    public class GetSubganreByGanreIdQuery : IRequest<List<SubganreViewModel>>
    {
        public int GanreId { get; set; }
    }
    public class GetSubganreByGanreIdQueryHandler : IRequestHandler<GetSubganreByGanreIdQuery, List<SubganreViewModel>>
    {
        private readonly ILibraryDbContext _context;
        private readonly IMapper _mapper;
        public GetSubganreByGanreIdQueryHandler(ILibraryDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<List<SubganreViewModel>> Handle(GetSubganreByGanreIdQuery request, CancellationToken cancellationToken)
        {
            var subganres = _context.Subganres
                                .Where(a => a.GanreId == request.GanreId)
                                    .AsNoTracking();

            return _mapper.Map<List<SubganreViewModel>>(subganres);
        }
    }
}

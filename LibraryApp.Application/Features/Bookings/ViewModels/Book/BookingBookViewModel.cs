﻿namespace LibraryApp.Application.Features.Bookings.ViewModels.Book
{
    public class BookingBookViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}

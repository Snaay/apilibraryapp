﻿using LibraryApp.Application.Features.Bookings.ViewModels.Book;

namespace LibraryApp.Application.Features.Bookings.ViewModels
{
    public class BaseBookingViewModel
    {
        public int Id { get; set; }
        public BookingBookViewModel Book { get; set; }
        public bool IsGiven { get; set; }
        public string BookingDate { get; set; }
        public string BookingEndDate { get; set; }
    }
}

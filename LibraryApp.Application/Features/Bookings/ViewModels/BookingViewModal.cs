﻿using LibraryApp.Application.Features.Books.ViewModels;

namespace LibraryApp.Application.Features.Bookings.ViewModels
{
    public class BookingViewModal : BaseBookingViewModel
    {
        public UserViewModel User { get; set; }
    }
}

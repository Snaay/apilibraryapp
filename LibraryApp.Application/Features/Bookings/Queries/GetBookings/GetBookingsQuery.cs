﻿using AutoMapper;
using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Features.Bookings.ViewModels;
using LibraryApp.Application.Helpers;
using LibraryApp.Application.Wrappers;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.Bookings.Queries.GetBookings
{
    public class GetBookingsQuery : IRequest<ListResponse<BookingViewModal>>
    {
        public string Search { get; set; }
        public int CurrentPage { get; set; } = 1;
        public int PageSize { get; set; } = 20;
    }
    public class GetBookingsQueryHandler : IRequestHandler<GetBookingsQuery, ListResponse<BookingViewModal>>
    {
        private readonly ILibraryDbContext _context;
        private readonly IMapper _mapper;
        public GetBookingsQueryHandler(ILibraryDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<ListResponse<BookingViewModal>> Handle(GetBookingsQuery request, CancellationToken cancellationToken)
        {
            var response = new ListResponse<BookingViewModal>();

            int skipStart = HelperMethods.GetSkipStartPagination(request.CurrentPage, request.PageSize);

            var searchItems = HelperMethods.SplitSearchString(request.Search);

            var bookings = _context.Bookings.
                                Include(a => a.Book)
                                    .Include(b => b.User)
                                .ThenInclude(p => p.UserProfile)
                                        .AsNoTracking()
                                            .AsQueryable();

            if(searchItems != null)
                foreach (var search in searchItems)
                    bookings = bookings.Where(a => a.Book.Name.ToLower().Contains(search) ||
                                                a.User.UserName.ToLower().Contains(search));

            response.TotalItemsCount = await bookings.CountAsync(cancellationToken);

            bookings = bookings.OrderByDescending(a => a.BookingDate).Skip(skipStart).Take(request.PageSize);

            response.Items = _mapper.Map<List<BookingViewModal>>(bookings);

            return response;
        }
    }
}

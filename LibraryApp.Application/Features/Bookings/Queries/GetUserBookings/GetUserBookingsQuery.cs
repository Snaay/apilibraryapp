﻿using AutoMapper;
using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Features.Bookings.ViewModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.Bookings.Queries.GetUserBookings
{
    public class GetUserBookingsQuery : IRequest<List<UserBookingViewModel>> { }
    public class GetUserBookingsQueryHandler : IRequestHandler<GetUserBookingsQuery, List<UserBookingViewModel>>
    {
        private readonly ILibraryDbContext _context;
        private readonly IMapper _mapper;
        private readonly ICurrentUserService _currentUserService;
        public GetUserBookingsQueryHandler(ILibraryDbContext context, IMapper mapper, ICurrentUserService currentUserService)
        {
            _context = context;
            _mapper = mapper;
            _currentUserService = currentUserService;
        }
        public async Task<List<UserBookingViewModel>> Handle(GetUserBookingsQuery request, CancellationToken cancellationToken)
        {
            var bookings = _context.Bookings.
                                Include(a => a.Book)
                                    .Where(a => a.UserId == _currentUserService.UserId)
                                        .OrderByDescending(a => a.BookingDate)
                                            .AsNoTracking();

            return _mapper.Map<List<UserBookingViewModel>>(bookings);
        }
    }
}

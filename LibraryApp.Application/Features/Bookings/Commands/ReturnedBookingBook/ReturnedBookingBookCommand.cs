﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Enums;
using LibraryApp.Application.Exceptions;
using LibraryApp.Application.Features.Books.Commands.SetQuantityBook;
using LibraryApp.Application.Wrappers;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.Bookings.Commands.ReturnedBook
{
    public class ReturnedBookingBookCommand : IRequest<Response<int>>
    {
        public int BookingId { get; set; }
    }
    public class ReturnedBookingBookCommandHandler : IRequestHandler<ReturnedBookingBookCommand, Response<int>>
    {
        private readonly ILibraryDbContext _context;
        private readonly IMediator _mediator;
        public ReturnedBookingBookCommandHandler(ILibraryDbContext context, IMediator mediator)
        {
            _context = context;
            _mediator = mediator;
        }
        public async Task<Response<int>> Handle(ReturnedBookingBookCommand request, CancellationToken cancellationToken)
        {
            var booking = await _context.Bookings
                .FirstOrDefaultAsync(a => a.Id == request.BookingId);

            if (booking == null)
                throw new ApiException("Бронь не найдена");

            await _mediator.Publish(new SetQuantityBookCommand // увеличиваем кол-во книг
            {
                BookId = booking.BookId,
                QuantityType = SetQuantityType.Inc
            }, cancellationToken);

            _context.Bookings.Remove(booking); // удаляем бронь

            await _context.SaveChangesAsync(cancellationToken);

            return new Response<int>(booking.Id, "Бронь удалена");
        }
    }
}

﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Enums;
using LibraryApp.Application.Exceptions;
using LibraryApp.Application.Features.Books.Commands.SetQuantityBook;
using LibraryApp.Application.Wrappers;
using LibraryApp.Domain.Entities;
using LibraryApp.Domain.Settings;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.Bookings.Commands.CreateBooking
{
    public class CreateBookingCommand : IRequest<Response<int>>
    {
        public int BookId { get; set; }
    }
    public class CreateBookingCommandHandler : IRequestHandler<CreateBookingCommand, Response<int>>
    {
        private readonly ILibraryDbContext _context;
        private readonly IMediator _mediator;
        private readonly ICurrentUserService _currentUserService;
        private readonly BookingSettings _bookingSettings;
        public CreateBookingCommandHandler(ILibraryDbContext context, IMediator mediator, ICurrentUserService currentUserService, IOptions<BookingSettings> bookingSettings)
        {
            _context = context;
            _mediator = mediator;
            _currentUserService = currentUserService;
            _bookingSettings = bookingSettings.Value;
        }
        public async Task<Response<int>> Handle(CreateBookingCommand request, CancellationToken cancellationToken)
        {
            var book = await _context.Books.FirstOrDefaultAsync(a => a.Id == request.BookId)
                ?? throw new ApiException("Книга не найдена");

            var bookings = _context.Bookings.Where(a => a.UserId == _currentUserService.UserId);

            if(await bookings.CountAsync() >= _bookingSettings.MaxNumberBookings)
                throw new ApiException("Вы достигли максимальное число бронирования книг");

            if (bookings.Any(a => a.BookId == request.BookId))
                throw new ApiException("Вы уже забронировали данную книгу");

            if (book.Quantity == 0)
                throw new ApiException("Книги все забранированны");

            var booking = new Booking
            {
                UserId = _currentUserService.UserId,
                Book = book,
                BookingDate = DateTime.Now
            };

            await _context.Bookings.AddAsync(booking);

            await _mediator.Publish(new SetQuantityBookCommand 
            { 
                BookId = request.BookId, 
                QuantityType = SetQuantityType.Dec 
            }, cancellationToken); // уменьшаем кол-во книг

            await _context.SaveChangesAsync(cancellationToken);

            return new Response<int>(booking.Id, "Бронь создана");
        }
    }
}

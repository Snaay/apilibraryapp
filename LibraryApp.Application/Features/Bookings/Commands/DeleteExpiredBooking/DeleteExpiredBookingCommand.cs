﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Enums;
using LibraryApp.Application.Features.Books.Commands.SetQuantityBook;
using LibraryApp.Domain.Entities;
using MediatR;
using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.Bookings.Commands.DeleteExpiredBooking
{
    public class DeleteExpiredBookingCommand : INotification
    {
        public Booking Booking { get; set; }
    }
    public class DeleteExpiredBookingCommandHandler : INotificationHandler<DeleteExpiredBookingCommand>
    {
        private readonly ILibraryDbContext _context;
        private readonly IMediator _mediator;
        private readonly ILogger<DeleteExpiredBookingCommand> _logger;
        public DeleteExpiredBookingCommandHandler(ILibraryDbContext context, IMediator mediator, ILogger<DeleteExpiredBookingCommand> logger)
        {
            _context = context;
            _mediator = mediator;
            _logger = logger;
        }
        public async Task Handle(DeleteExpiredBookingCommand request, CancellationToken cancellationToken)
        {
            await _mediator.Publish(new SetQuantityBookCommand // увеличиваем кол-во книг
            {
                BookId = request.Booking.BookId,
                QuantityType = SetQuantityType.Inc
            }, cancellationToken);

            _context.Bookings.Remove(request.Booking); // удаляем бронь

            await _context.SaveChangesAsync(cancellationToken);

            _logger.LogInformation($"Бронь с Id: {request.Booking.Id} удалена.");

            await Task.CompletedTask;
        }
    }
}

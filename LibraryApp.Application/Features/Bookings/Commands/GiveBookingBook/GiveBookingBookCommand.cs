﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Exceptions;
using LibraryApp.Application.Wrappers;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.Bookings.Commands.GiveBookingBook
{
    public class GiveBookingBookCommand : IRequest<Response<int>>
    {
        public int BookingId { get; set; }
    }
    public class GiveBookingBookCommandHandler : IRequestHandler<GiveBookingBookCommand, Response<int>>
    {
        private readonly ILibraryDbContext _context;
        public GiveBookingBookCommandHandler(ILibraryDbContext context) => _context = context;
        public async Task<Response<int>> Handle(GiveBookingBookCommand request, CancellationToken cancellationToken)
        {
            var booking = await _context.Bookings.FirstOrDefaultAsync(a => a.Id == request.BookingId)
                ?? throw new ApiException("Бронь не найдена.");

            booking.IsGiven = true;

            await _context.SaveChangesAsync(cancellationToken);

            return new Response<int>(booking.Id, "Книга выдана");
        }
    }
}

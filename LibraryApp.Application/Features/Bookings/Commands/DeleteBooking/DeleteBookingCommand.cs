﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Enums;
using LibraryApp.Application.Exceptions;
using LibraryApp.Application.Features.Books.Commands.SetQuantityBook;
using LibraryApp.Application.Wrappers;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.Bookings.Commands.DeleteBooking
{
    public class DeleteBookingCommand : IRequest<Response<int>>
    {
        public int BookingId { get; set; }
    }
    public class DeleteBookingCommandHandler : IRequestHandler<DeleteBookingCommand, Response<int>>
    {
        private readonly ILibraryDbContext _context;
        private readonly ICurrentUserService _currentUserService;
        private readonly IMediator _mediator;
        public DeleteBookingCommandHandler(ILibraryDbContext context, ICurrentUserService currentUserService, IMediator mediator)
        {
            _context = context;
            _currentUserService = currentUserService;
            _mediator = mediator;
        }
        public async Task<Response<int>> Handle(DeleteBookingCommand request, CancellationToken cancellationToken)
        {
            var booking = await _context.Bookings
                .FirstOrDefaultAsync(a => a.Id == request.BookingId && a.UserId == _currentUserService.UserId)
                    ?? throw new ApiException("Бронь не найдена");

            if (booking.IsGiven)
                throw new ApiException("Невозможно снять бронь, так как книга выдана");

            await _mediator.Publish(new SetQuantityBookCommand // увеличиваем кол-во книг
            { 
                BookId = booking.BookId, 
                QuantityType = SetQuantityType.Inc 
            }, cancellationToken); 

            _context.Bookings.Remove(booking); // удаляем бронь

            await _context.SaveChangesAsync(cancellationToken);

            return new Response<int>(booking.Id, "Бронь удалена");
        }
    }
}
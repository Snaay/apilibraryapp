﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Exceptions;
using LibraryApp.Application.Wrappers;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.SubNotifications.Commands.DeleteSubNotification
{
    public class DeleteSubNotificationCommand : IRequest<Response<int>>
    {
        public int BookId { get; set; }
    }
    public class DeleteSubNotificationCommandHandler : IRequestHandler<DeleteSubNotificationCommand, Response<int>>
    {
        private readonly ILibraryDbContext _context;
        private readonly ICurrentUserService _currentUserService;
        public DeleteSubNotificationCommandHandler(ILibraryDbContext context, ICurrentUserService currentUserService)
        {
            _context = context;
            _currentUserService = currentUserService;
        }
        public async Task<Response<int>> Handle(DeleteSubNotificationCommand request, CancellationToken cancellationToken)
        {
            var book = await _context.Books.FirstOrDefaultAsync(a => a.Id == request.BookId)
                ?? throw new ApiException("Книга не найдена");

            var subNotification = await _context.SubNotifications
                .FirstOrDefaultAsync(a => a.BookId == request.BookId && a.UserId == _currentUserService.UserId)
                    ?? throw new ApiException("Подписка на уведомление не найдена");

            _context.SubNotifications.Remove(subNotification);

            await _context.SaveChangesAsync(cancellationToken);

            return new Response<int>(subNotification.Id, "Подписка на уведомление удалено");
        }
    }
}

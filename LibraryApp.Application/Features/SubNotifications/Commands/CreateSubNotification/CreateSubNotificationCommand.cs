﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Exceptions;
using LibraryApp.Application.Wrappers;
using LibraryApp.Domain.Entities.Notification;
using LibraryApp.Domain.Enums;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.SubNotifications.Commands.CreateSubNotification
{
    public class CreateSubNotificationCommand : IRequest<Response<int>>
    {
        public int BookId { get; set; }
        public int NotificationType { get; set; }
    }
    public class CreateSubNotificationCommandHandler : IRequestHandler<CreateSubNotificationCommand, Response<int>>
    {
        private readonly ILibraryDbContext _context;
        private readonly ICurrentUserService _currentUserService;
        public CreateSubNotificationCommandHandler(ILibraryDbContext context, ICurrentUserService currentUserService)
        {
            _context = context;
            _currentUserService = currentUserService;
        }
        public async Task<Response<int>> Handle(CreateSubNotificationCommand request, CancellationToken cancellationToken)
        {
            var book = await _context.Books.FirstOrDefaultAsync(a => a.Id == request.BookId)
                ?? throw new ApiException("Книга не найдена");

            if (book.Quantity > 0)
                throw new ApiException("Книга доступна для бронирования");

            var subNotification = await _context.SubNotifications.FirstOrDefaultAsync(a => a.BookId == request.BookId && a.UserId == _currentUserService.UserId);

            if (subNotification != null)
                throw new ApiException("Вы уже подписаны на уведомление");

            subNotification = new SubNotification
            {
                UserId = _currentUserService.UserId,
                BookId = book.Id,
                SubNotificationTypeId = (SubNotificationType)request.NotificationType
            };

            await _context.SubNotifications.AddAsync(subNotification);

            await _context.SaveChangesAsync(cancellationToken);

            return new Response<int>(subNotification.Id, "Подписка на уведомление добавлена");
        }
    }
}

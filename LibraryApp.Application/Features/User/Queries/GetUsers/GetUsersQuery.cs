﻿using AutoMapper;
using LibraryApp.Application.Features.User.ViewModals;
using LibraryApp.Application.Wrappers;
using LibraryApp.Domain.Entities.User;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.User.Queries.GetUsers
{
    public class GetUsersQuery : IRequest<ListResponse<UserListViewModel>>
    {
        public string Search { get; set; }
        public int CurrentPage { get; set; } = 1;
        public int PageSize { get; set; } = 20;
    }
    public class GetUsersQueryHandler : IRequestHandler<GetUsersQuery, ListResponse<UserListViewModel>>
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IMapper _mapper;
        public GetUsersQueryHandler(UserManager<ApplicationUser> userManager, IMapper mapper)
        {
            _mapper = mapper;
            _userManager = userManager;
        }
        public async Task<ListResponse<UserListViewModel>> Handle(GetUsersQuery request, CancellationToken cancellationToken)
        {
            var response = new ListResponse<UserListViewModel>();

            int skipStart = request.CurrentPage == 1 ? 0 : (request.CurrentPage * request.PageSize) - request.PageSize;

            var searchItems = request.Search?.ToLower().Split(' ')
                                 .Where(a => !string.IsNullOrEmpty(a)).ToArray();

            var users = _userManager
                            .Users.Include(p => p.UserProfile)
                                .AsNoTracking();

            if(searchItems != null)
                foreach (var search in searchItems)
                    users = _userManager.Users
                        .Include(p => p.UserProfile)
                            .Where(a => a.UserName.ToLower().Contains(search) ||
                                a.UserProfile.FirstName.ToLower().Contains(search) ||
                                    a.UserProfile.LastName.ToLower().Contains(search) ||
                                        a.UserProfile.MiddleName.ToLower().Contains(search));

            response.TotalItemsCount = await users.CountAsync(cancellationToken);

            users = users.OrderByDescending(a => a.DateCreated).Skip(skipStart).Take(request.PageSize);

            response.Items = _mapper.Map<List<UserListViewModel>>(users);

            return response;
        }
    }
}

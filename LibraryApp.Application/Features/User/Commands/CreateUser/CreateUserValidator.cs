﻿using FluentValidation;
using LibraryApp.Application.Features.Validators;

namespace LibraryApp.Application.Features.User.Commands.CreateUser
{
    public class CreateUserValidator : AbstractValidator<CreateUserCommand>
    {
        public CreateUserValidator()
        {
            RuleFor(x => x.Email)
                .Email();
            RuleFor(x => x.UserName)
                .Login();
            RuleFor(x => x.Password)
                .Password()
                .Equal(a => a.ConfirmPassword)
                .WithMessage("Пароли не совпадают");
            RuleFor(x => x.FirstName).NotEmpty().WithMessage("Имя обязательно для заполнения");
            RuleFor(x => x.LastName).NotEmpty().WithMessage("Фамилия обязательно для заполнения");
        }
    }
}

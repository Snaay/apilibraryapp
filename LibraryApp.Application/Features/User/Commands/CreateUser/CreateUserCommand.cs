﻿using LibraryApp.Application.Enums;
using LibraryApp.Application.Exceptions;
using LibraryApp.Application.Wrappers;
using LibraryApp.Domain.Entities.User;
using MediatR;
using Microsoft.AspNetCore.Identity;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.User.Commands.CreateUser
{
    public class CreateUserCommand : IRequest<Response<string>>
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
    }
    public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand, Response<string>>
    {
        private readonly UserManager<ApplicationUser> _userManager;
        public CreateUserCommandHandler(UserManager<ApplicationUser> userManager) => _userManager = userManager;
        public async Task<Response<string>> Handle(CreateUserCommand request, CancellationToken cancellationToken)
        {
            var user = new ApplicationUser
            {
                UserName = request.UserName,
                Email = request.Email,
                EmailConfirmed = true,
                DateCreated = DateTime.Now,
                UserProfile = new UserProfile
                {
                    FirstName = request.FirstName,
                    LastName = request.LastName,
                    MiddleName = request.MiddleName
                }
            };

            var result = await _userManager.CreateAsync(user, request.Password);

            if (!result.Succeeded)
                throw new ApiValidationException(result.Errors);

            await _userManager.AddToRoleAsync(user, Roles.Customer.ToString());

            return new Response<string>(null, $"Пользователь {request.UserName} создан");
        }
    }
}

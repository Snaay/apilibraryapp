﻿using LibraryApp.Application.Exceptions;
using LibraryApp.Application.Wrappers;
using LibraryApp.Domain.Entities.User;
using MediatR;
using Microsoft.AspNetCore.Identity;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.User.Commands.SetPassword
{
    public class SetPasswordCommand : IRequest<Response<string>>
    {
        public string UserId { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }
    public class SetPasswordCommandHandler : IRequestHandler<SetPasswordCommand, Response<string>>
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IPasswordValidator<ApplicationUser> _passwordValidator;
        private readonly IPasswordHasher<ApplicationUser> _passwordHasher;
        public SetPasswordCommandHandler(UserManager<ApplicationUser> userManager, IPasswordValidator<ApplicationUser> passwordValidator, IPasswordHasher<ApplicationUser> passwordHasher)
        {
            _userManager = userManager;
            _passwordValidator = passwordValidator;
            _passwordHasher = passwordHasher;
        }
        public async Task<Response<string>> Handle(SetPasswordCommand request, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByIdAsync(request.UserId)
                ?? throw new ApiException("Пользователь не найден");

            var result = await _passwordValidator.ValidateAsync(_userManager, user, request.Password);

            if (!result.Succeeded)
                throw new ApiValidationException(result.Errors);

            user.PasswordHash = _passwordHasher.HashPassword(user, request.Password);

            await _userManager.UpdateAsync(user);

            return new Response<string>(null, $"Пароль изменен для {user.UserName}");
        }
    }
}

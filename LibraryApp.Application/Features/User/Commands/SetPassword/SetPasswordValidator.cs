﻿using FluentValidation;
using LibraryApp.Application.Features.Validators;

namespace LibraryApp.Application.Features.User.Commands.SetPassword
{
    public class SetPasswordValidator : AbstractValidator<SetPasswordCommand>
    {
        public SetPasswordValidator()
        {
            RuleFor(x => x.UserId).NotEmpty().WithMessage("Id пользователя отсутствует");
            RuleFor(x => x.Password).NotEmpty().WithMessage("Пароль обязательный для заполнения");
            RuleFor(x => x.ConfirmPassword).Password()
                .Equal(a => a.Password)
                .WithMessage("Пароли не совпадают");
        }
    }
}

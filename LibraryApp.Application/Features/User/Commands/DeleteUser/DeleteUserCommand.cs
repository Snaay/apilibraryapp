﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Common.Interfaces.File;
using LibraryApp.Application.DTO.File;
using LibraryApp.Application.Exceptions;
using LibraryApp.Application.Features.Bookings.Commands.DeleteExpiredBooking;
using LibraryApp.Application.Wrappers;
using LibraryApp.Domain.Entities.User;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.User.Commands.DeleteUser
{
    public class DeleteUserCommand : IRequest<Response<string>>
    {
        public string UserId { get; set; }
    }
    public class CreateUserCommandHandler : IRequestHandler<DeleteUserCommand, Response<string>>
    {
        private readonly ILibraryDbContext _context;
        private readonly IFileService _fileService;
        private readonly IMediator _mediator;
        private readonly UserManager<ApplicationUser> _userManager;
        public CreateUserCommandHandler(ILibraryDbContext context, IFileService fileService, IMediator mediator, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _fileService = fileService;
            _mediator = mediator;
            _userManager = userManager;
        }
        public async Task<Response<string>> Handle(DeleteUserCommand request, CancellationToken cancellationToken)
        {
            var user = await _context.Users
                        .Include(p => p.UserProfile)
                            .Include(c => c.Comments)
                                .Include(b => b.Bookings)
                                    .Include(s => s.SubNotifications)
                                        .Include(r => r.ReceiveNotifications)
                                            .FirstOrDefaultAsync(a => a.Id == request.UserId)
                                             ?? throw new ApiException("Пользователь не найден");

            while (user.Bookings.Count > 0)
                await _mediator.Publish(new DeleteExpiredBookingCommand 
                { 
                    Booking = user.Bookings.First() 
                }, cancellationToken);

            if (!string.IsNullOrEmpty(user.UserProfile.PhotoPath))
                await _fileService.DeleteFile(new DeleteFileRequest
                {
                    FilePath = user.UserProfile.PhotoPath
                });

            var result = await _userManager.DeleteAsync(user);

            if (!result.Succeeded)
                throw new ApiValidationException(result.Errors);

            return new Response<string>(null, $"Пользователь {user.UserName} удален");
        }
    }
}

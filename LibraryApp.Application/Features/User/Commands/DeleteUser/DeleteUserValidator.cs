﻿using FluentValidation;

namespace LibraryApp.Application.Features.User.Commands.DeleteUser
{
    public class DeleteUserValidator : AbstractValidator<DeleteUserCommand>
    {
        public DeleteUserValidator()
        {
            RuleFor(x => x.UserId).NotEmpty().WithMessage("Id пользователя отсутствует");
        }
    }
}

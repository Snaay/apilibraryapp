﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Exceptions;
using LibraryApp.Application.Wrappers;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.Ganres.Commands.DeleteGanre
{
    public class DeleteGanreCommand : IRequest<Response<int>>
    {
        public int GanreId { get; set; }
    }
    public class DeleteGanreCommandHandler : IRequestHandler<DeleteGanreCommand, Response<int>>
    {
        private readonly ILibraryDbContext _context;
        public DeleteGanreCommandHandler(ILibraryDbContext context) => _context = context;
        public async Task<Response<int>> Handle(DeleteGanreCommand request, CancellationToken cancellationToken)
        {
            var ganre = await _context.Ganres.FirstOrDefaultAsync(a => a.Id == request.GanreId)
                ?? throw new ApiException("Жанр не найден");

            _context.Ganres.Remove(ganre);

            await _context.SaveChangesAsync(cancellationToken);

            return new Response<int>(ganre.Id, "Жанр удален");
        }
    }
}

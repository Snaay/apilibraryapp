﻿using FluentValidation;

namespace LibraryApp.Application.Features.Ganres.Commands.DeleteGanre
{
    public class DeleteGanreCommandValidator : AbstractValidator<DeleteGanreCommand>
    {
        public DeleteGanreCommandValidator()
        {
            RuleFor(a => a.GanreId).NotEmpty().WithMessage("Id жарнра отсутствует");
        }
    }
}

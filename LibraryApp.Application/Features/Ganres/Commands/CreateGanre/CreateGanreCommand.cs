﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Wrappers;
using LibraryApp.Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.Ganres.Commands.CreateGanre
{
    public class CreateGanreCommand : IRequest<Response<int>>
    {
        public string Name { get; set; }
    }
    public class CreateGanreCommandHandler : IRequestHandler<CreateGanreCommand, Response<int>>
    {
        private readonly ILibraryDbContext _context;
        public CreateGanreCommandHandler(ILibraryDbContext context) => _context = context;
        public async Task<Response<int>> Handle(CreateGanreCommand request, CancellationToken cancellationToken)
        {
            var ganre = await _context.Ganres.AddAsync(new Ganre { Name = request.Name });

            await _context.SaveChangesAsync(cancellationToken);

            return new Response<int>(ganre.Entity.Id, "Жанр добавлен");
        }
    }
}

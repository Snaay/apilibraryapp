﻿using FluentValidation;

namespace LibraryApp.Application.Features.Ganres.Commands.CreateGanre
{
    public class CreateGanreCommandValidator : AbstractValidator<CreateGanreCommand>
    {
        public CreateGanreCommandValidator()
        {
            RuleFor(a => a.Name).NotEmpty().WithMessage("Имя жанра обязательно для заполнения");
        }
    }
}

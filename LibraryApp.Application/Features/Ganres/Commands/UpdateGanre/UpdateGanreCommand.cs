﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Exceptions;
using LibraryApp.Application.Wrappers;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.Ganres.Commands.UpdateGanre
{
    public class UpdateGanreCommand : IRequest<Response<int>>
    {
        public int GanreId { get; set; }
        public string Name { get; set; }
    }
    public class UpdateGanreCommandHandler : IRequestHandler<UpdateGanreCommand, Response<int>>
    {
        private readonly ILibraryDbContext _context;
        public UpdateGanreCommandHandler(ILibraryDbContext context) => _context = context;
        public async Task<Response<int>> Handle(UpdateGanreCommand request, CancellationToken cancellationToken)
        {
            var ganre = await _context.Ganres.FirstOrDefaultAsync(a => a.Id == request.GanreId)
                ?? throw new ApiException("Жанр не найден");

            ganre.Name = request.Name;

            await _context.SaveChangesAsync(cancellationToken);

            return new Response<int>(ganre.Id, "Жанр обновлен");
        }
    }
}

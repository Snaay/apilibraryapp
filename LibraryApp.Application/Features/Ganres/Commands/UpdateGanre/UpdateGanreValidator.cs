﻿using FluentValidation;

namespace LibraryApp.Application.Features.Ganres.Commands.UpdateGanre
{
    public class UpdateGanreValidator : AbstractValidator<UpdateGanreCommand>
    {
        public UpdateGanreValidator()
        {
            RuleFor(a => a.GanreId).NotEmpty().WithMessage("Id жарнра отсутствует");
            RuleFor(a => a.Name).NotEmpty().WithMessage("Имя жанра обязательно для заполнения");
        }
    }
}

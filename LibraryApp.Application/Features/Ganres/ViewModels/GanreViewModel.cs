﻿using LibraryApp.Application.Features.Subganres.ViewModels;
using System.Collections.Generic;

namespace LibraryApp.Application.Features.Ganres.ViewModels
{
    public class GanreViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<SubganreWithCountBooksViewModel> Subganres { get; set; }
    }
}

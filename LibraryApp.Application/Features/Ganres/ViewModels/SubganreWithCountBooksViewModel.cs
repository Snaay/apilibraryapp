﻿namespace LibraryApp.Application.Features.Ganres.ViewModels
{
    public class SubganreWithCountBooksViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CountBooks { get; set; }
    }
}

﻿using AutoMapper;
using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Features.Ganres.ViewModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.Ganres.Queries.GetGanres
{
    public class GetGanresQuery : IRequest<List<GanreViewModel>> { }
    public class GetSubganreByGanreIdQueryHandler : IRequestHandler<GetGanresQuery, List<GanreViewModel>>
    {
        private readonly ILibraryDbContext _context;
        private readonly IMapper _mapper;
        public GetSubganreByGanreIdQueryHandler(ILibraryDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<List<GanreViewModel>> Handle(GetGanresQuery request, CancellationToken cancellationToken)
        {
            var ganres = await _context.Ganres.Select(ganre => new GanreViewModel
            {
                Id = ganre.Id,
                Name = ganre.Name,
                Subganres = ganre.Subganres.Select(subganre => new SubganreWithCountBooksViewModel 
                { 
                    Id = subganre.Id,
                    Name = subganre.Name,
                    CountBooks = subganre.GanreBooks.Count
                }).OrderBy(a => a.Name).ToList()
            }).OrderBy(a => a.Name).ToListAsync();

            return ganres;
        }
    }
}

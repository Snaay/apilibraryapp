﻿using AutoMapper;
using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Features.Notifications.ViewModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.Notifications.Queries.GetNotification
{
    public class GetNotificationsQuery : IRequest<List<NotificationViewModel>> { }
    public class GetNotificationsQueryHandler : IRequestHandler<GetNotificationsQuery, List<NotificationViewModel>>
    {
        private readonly ILibraryDbContext _context;
        private readonly IMapper _mapper;
        private readonly ICurrentUserService _currentUserService;
        public GetNotificationsQueryHandler(ILibraryDbContext context, IMapper mapper, ICurrentUserService currentUserService)
        {
            _context = context;
            _mapper = mapper;
            _currentUserService = currentUserService;
        }
        public async Task<List<NotificationViewModel>> Handle(GetNotificationsQuery request, CancellationToken cancellationToken)
        {
            var notifications = _context.ReceiveNotifications
                                    .Include(a => a.User)
                                        .Include(b => b.Notification)
                                        .ThenInclude(c => c.Book)
                                            .Where(a => a.UserId == _currentUserService.UserId)
                                                .OrderByDescending(d => d.Notification.ArrivalDate)
                                                    .AsNoTracking();

            return _mapper.Map<List<NotificationViewModel>>(notifications);
        }
    }
}

﻿namespace LibraryApp.Application.Features.Notifications.ViewModels
{
    public class NotificationViewModel
    {
        public bool IsRead { get; set; }
        public string Message { get; set; }
        public int BookId { get; set; }
        public string ArrivalDate { get; set; }
    }
}

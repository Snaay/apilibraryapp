﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Wrappers;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.Notifications.Commands.CheckNotfications
{
    public class CheckNotificationCommand : IRequest<Response<int>> { }
    public class CheckNotificationCommandHandler : IRequestHandler<CheckNotificationCommand, Response<int>>
    {
        private readonly ILibraryDbContext _context;
        private readonly ICurrentUserService _currentUserService;
        public CheckNotificationCommandHandler(ILibraryDbContext context, ICurrentUserService currentUserService)
        {
            _context = context;
            _currentUserService = currentUserService;
        }
        public async Task<Response<int>> Handle(CheckNotificationCommand request, CancellationToken cancellationToken)
        {
            var receiveNotifications = _context.ReceiveNotifications
                                            .Include(u => u.User)
                                                .AsNoTracking()
                                                    .Where(a => a.UserId == _currentUserService.UserId && a.IsRead == false);

            return new Response<int>(receiveNotifications.Count());
        }
    }
}

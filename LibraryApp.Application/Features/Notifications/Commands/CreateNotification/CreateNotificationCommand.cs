﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Common.NotificationMessage;
using LibraryApp.Application.DTO.Email;
using LibraryApp.Domain.Entities.Notification;
using LibraryApp.Domain.Enums;
using LibraryApp.Domain.Settings;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.Notifications.Commands.CreateNotification
{
    public class CreateNotificationCommand : INotification
    {
        public int BookId { get; set; }
    }
    public class CreateNotificationCommandHandler : INotificationHandler<CreateNotificationCommand>
    {
        private readonly ILibraryDbContext _context;
        private readonly IEmailService _emailService;
        private readonly MailSettings _mailSettings;
        public CreateNotificationCommandHandler(ILibraryDbContext context, IEmailService emailService, IOptions<MailSettings> mailSettings)
        {
            _context = context;
            _emailService = emailService;
            _mailSettings = mailSettings.Value;
        }
        public async Task Handle(CreateNotificationCommand request, CancellationToken cancellationToken)
        {
            var book = await _context.Books
                .Include(s => s.SubNotifications)
                    .ThenInclude(u => u.User)
                        .FirstOrDefaultAsync(b => b.Id == request.BookId);

            if (book.SubNotifications.Count == 0)
                await Task.CompletedTask;

            var notification = new Notification
            {
                BookId = book.Id,
                ArrivalDate = DateTime.Now
            };

            var result = await _context.Notifications.AddAsync(notification, cancellationToken); // добавляем общее уведомление по книги

            notification = result.Entity;

            foreach (var sub in book.SubNotifications) // добавляем в общее уведомление пользователей которые подписались на книгу или отправляем уведомление на почту пользователя
            {
                switch (sub.SubNotificationTypeId)
                {
                    case SubNotificationType.Service:
                        await _context.ReceiveNotifications.AddAsync(new ReceiveNotification
                        {
                            Notification = notification,
                            UserId = sub.UserId
                        }, cancellationToken);
                        break;
                    case SubNotificationType.Email:
                        await _emailService.SendAsync(new EmailRequest
                        {
                            From = _mailSettings.EmailFrom,
                            Body = NotificationMessage.GetMessage(book.Name),
                            Subject = "Бронирование книги",
                            To = sub.User.Email
                        });
                        break;
                }
            }
            // уведомления добавлены, теперь удаляем все подписки на книгу
            _context.SubNotifications.RemoveRange(book.SubNotifications);

            await _context.SaveChangesAsync(cancellationToken);

            await Task.CompletedTask;
        }
    }
}

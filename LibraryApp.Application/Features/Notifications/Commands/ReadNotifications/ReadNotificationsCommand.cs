﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Wrappers;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.Notifications.Commands.ReadNotifications
{
    public class ReadNotificationsCommand : IRequest<Response<string>> { }
    public class ReadNotificationsCommandHandler : IRequestHandler<ReadNotificationsCommand, Response<string>>
    {
        private readonly ILibraryDbContext _context;
        private readonly ICurrentUserService _currentUserService;
        public ReadNotificationsCommandHandler(ILibraryDbContext context, ICurrentUserService currentUserService)
        {
            _context = context;
            _currentUserService = currentUserService;
        }
        public async Task<Response<string>> Handle(ReadNotificationsCommand request, CancellationToken cancellationToken)
        {
            var receiveNotifications = _context.ReceiveNotifications
                                            .Include(u => u.User)
                                                .Where(a => a.UserId == _currentUserService.UserId);

            if (receiveNotifications != null)
                foreach (var receiveNotification in receiveNotifications)
                    receiveNotification.IsRead = true;

            await _context.SaveChangesAsync(cancellationToken);

            return new Response<string>(null, null);
        }
    }
}

﻿using FluentValidation;

namespace LibraryApp.Application.Features.Authors.Commands.DeleteAuthor
{
    public class DeleteAuthorValidator : AbstractValidator<DeleteAuthorCommand>
    {
        public DeleteAuthorValidator()
        {
            RuleFor(a => a.AuthorId).NotEmpty().WithMessage("Id автора отсутствует");
        }
    }
}

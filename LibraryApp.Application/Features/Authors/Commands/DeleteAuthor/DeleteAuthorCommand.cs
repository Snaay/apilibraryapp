﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Exceptions;
using LibraryApp.Application.Wrappers;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.Authors.Commands.DeleteAuthor
{
    public class DeleteAuthorCommand : IRequest<Response<int>>
    {
        public int AuthorId { get; set; }
    }
    public class DeleteAuthorCommandHandler : IRequestHandler<DeleteAuthorCommand, Response<int>>
    {
        private readonly ILibraryDbContext _context;
        public DeleteAuthorCommandHandler(ILibraryDbContext context) => _context = context;
        public async Task<Response<int>> Handle(DeleteAuthorCommand request, CancellationToken cancellationToken)
        {
            var author = await _context.Authors.FirstOrDefaultAsync(a => a.Id == request.AuthorId) 
                ?? throw new ApiException("Автор не найден");

            _context.Authors.Remove(author);

            await _context.SaveChangesAsync(cancellationToken);

            return new Response<int>(author.Id, "Автор удален");
        }
    }
}

﻿using AutoMapper;
using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Wrappers;
using LibraryApp.Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.Authors.Commands.CreateAuthor
{
    public class CreateAuthorCommand : IRequest<Response<int>>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
    }
    public class CreateAuthorCommandHandler : IRequestHandler<CreateAuthorCommand, Response<int>>
    {
        private readonly ILibraryDbContext _context;
        private readonly IMapper _mapper;
        public CreateAuthorCommandHandler(ILibraryDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<Response<int>> Handle(CreateAuthorCommand request, CancellationToken cancellationToken)
        {
            var author = await _context.Authors.AddAsync(_mapper.Map<Author>(request));

            await _context.SaveChangesAsync(cancellationToken);

            return new Response<int>(author.Entity.Id, "Автор добавлен");
        }
    }
}

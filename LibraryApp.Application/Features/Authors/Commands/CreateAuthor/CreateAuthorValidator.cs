﻿using FluentValidation;

namespace LibraryApp.Application.Features.Authors.Commands.CreateAuthor
{
    public class CreateAuthorValidator : AbstractValidator<CreateAuthorCommand>
    {
        public CreateAuthorValidator()
        {
            RuleFor(a => a.FirstName).NotNull().WithMessage("Имя автора обязательно для заполенения");
            RuleFor(a => a.LastName).NotNull().WithMessage("Фамилия автора обязательно для заполенения");
        }
    }
}

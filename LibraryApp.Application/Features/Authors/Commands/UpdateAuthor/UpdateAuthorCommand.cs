﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Exceptions;
using LibraryApp.Application.Wrappers;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.Authors.Commands.UpdateAuthor
{
    public class UpdateAuthorCommand : IRequest<Response<int>>
    {
        public int AuthorId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
    }
    public class UpdateAuthorCommandHandler : IRequestHandler<UpdateAuthorCommand, Response<int>>
    {
        private readonly ILibraryDbContext _context;
        public UpdateAuthorCommandHandler(ILibraryDbContext context) => _context = context;
        public async Task<Response<int>> Handle(UpdateAuthorCommand request, CancellationToken cancellationToken)
        {
            var author = await _context.Authors.FirstOrDefaultAsync(a => a.Id == request.AuthorId)
                ?? throw new ApiException("Автор не найден");

            author.FirstName = request.FirstName;
            author.LastName = request.LastName;
            author.MiddleName = request.MiddleName;

            await _context.SaveChangesAsync(cancellationToken);

            return new Response<int>(author.Id, "Автор обновлен");
        }
    }
}

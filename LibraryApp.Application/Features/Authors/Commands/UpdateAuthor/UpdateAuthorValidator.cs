﻿using FluentValidation;

namespace LibraryApp.Application.Features.Authors.Commands.UpdateAuthor
{
    public class UpdateAuthorValidator : AbstractValidator<UpdateAuthorCommand>
    {
        public UpdateAuthorValidator()
        {
            RuleFor(a => a.AuthorId).NotEmpty().WithMessage("Id автора отсутствует");
            RuleFor(a => a.FirstName).NotNull().WithMessage("Имя автора обязательно для заполенения");
            RuleFor(a => a.LastName).NotNull().WithMessage("Фамилия автора обязательно для заполенения");
        }
    }
}

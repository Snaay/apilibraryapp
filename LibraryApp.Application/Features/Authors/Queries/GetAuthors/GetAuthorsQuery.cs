﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Features.Authors.ViewModels;
using LibraryApp.Application.Helpers;
using LibraryApp.Application.Wrappers;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.Authors.Queries.FilterAuthorSelection
{
    public class GetAuthorsQuery : IRequest<ListResponse<AuthorViewModel>>
    {
        public string Search { get; set; }
        public int CurrentPage { get; set; } = 1;
        public int PageSize { get; set; } = 20;
    }
    public class FilterAuthorsQueryHandler : IRequestHandler<GetAuthorsQuery, ListResponse<AuthorViewModel>>
    {
        private readonly ILibraryDbContext _context;
        public FilterAuthorsQueryHandler(ILibraryDbContext context)
        {
            _context = context;
        }
        public async Task<ListResponse<AuthorViewModel>> Handle(GetAuthorsQuery request, CancellationToken cancellationToken)
        {
            var response = new ListResponse<AuthorViewModel>();

            int skipStart = HelperMethods.GetSkipStartPagination(request.CurrentPage, request.PageSize);

            var searchItems = HelperMethods.SplitSearchString(request.Search);

            var authors = _context.Authors.Select(a => new AuthorViewModel
            {
                Id = a.Id,
                FirstName = a.FirstName,
                LastName = a.LastName,
                MiddleName = a.MiddleName,
                CountBooks = a.Books.Count
            }).AsNoTracking();

            if (searchItems != null)
                foreach (var search in searchItems)
                    authors = authors.Where(a =>
                                a.FirstName.ToLower().Contains(search) ||
                                    a.LastName.ToLower().Contains(search) ||
                                        a.MiddleName.ToLower().Contains(search));

            response.TotalItemsCount = await authors.CountAsync(cancellationToken);

            response.Items = authors.Skip(skipStart).Take(request.PageSize);

            return response;
        }
    }
}

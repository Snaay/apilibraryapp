﻿namespace LibraryApp.Application.Features.Authors.ViewModels
{
    public class AuthorViewModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public int CountBooks { get; set; }
    }
}

﻿using FluentValidation;

namespace LibraryApp.Application.Features.UserProfiles.Commands.UpdateUserProfile
{
    public class UpdateUserProfileValidator : AbstractValidator<UpdateUserProfileCommand>
    {
        public UpdateUserProfileValidator()
        {
            RuleFor(x => x.FirstName).NotEmpty().WithMessage("Имя обязательно для заполнения");
            RuleFor(x => x.FirstName).NotEmpty().WithMessage("Фамилия обязательно для заполнения");
        }
    }
}

﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Exceptions;
using LibraryApp.Application.Wrappers;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.UserProfiles.Commands.UpdateUserProfile
{
    public class UpdateUserProfileCommand : IRequest<Response<int>>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
    }
    public class UpdateUserProfileCommandHandler : IRequestHandler<UpdateUserProfileCommand, Response<int>>
    {
        private readonly ILibraryDbContext _context;
        private readonly ICurrentUserService _currentUserService;
        public UpdateUserProfileCommandHandler(ILibraryDbContext context, ICurrentUserService currentUserService)
        {
            _context = context;
            _currentUserService = currentUserService;
        }
        public async Task<Response<int>> Handle(UpdateUserProfileCommand request, CancellationToken cancellationToken)
        {
            var userProfile = await _context.UserProfiles
                .FirstOrDefaultAsync(a => a.UserId == _currentUserService.UserId)
                    ?? throw new ApiException("Профиль не найден");


            userProfile.FirstName = request.FirstName;
            userProfile.LastName = request.LastName;
            userProfile.MiddleName = request.MiddleName;

            _context.UserProfiles.Update(userProfile);

            await _context.SaveChangesAsync(cancellationToken);

            return new Response<int>(userProfile.Id, "Профиль обновлен");
        }
    }
}

﻿using AutoMapper;
using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Exceptions;
using LibraryApp.Application.Features.UserProfiles.ViewModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.UserProfiles.Queries.GetUserProfile
{
    public class GetUserProfileQuery : IRequest<UserProfileViewModel> { }
    public class GetUserProfileQueryHandler : IRequestHandler<GetUserProfileQuery, UserProfileViewModel>
    {
        private readonly ILibraryDbContext _context;
        private readonly ICurrentUserService _currentUserService;
        private readonly IMapper _mapper;
        public GetUserProfileQueryHandler(ILibraryDbContext context, ICurrentUserService currentUserService, IMapper mapper)
        {
            _context = context;
            _currentUserService = currentUserService;
            _mapper = mapper;
        }
        public async Task<UserProfileViewModel> Handle(GetUserProfileQuery request, CancellationToken cancellationToken)
        {
            var userProfile = await _context.UserProfiles
                                        .FirstOrDefaultAsync(a => a.UserId == _currentUserService.UserId);

            if (userProfile == null)
                throw new ApiException("Профиль не найден");

            return _mapper.Map<UserProfileViewModel>(userProfile);
        }
    }
}

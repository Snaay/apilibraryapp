﻿namespace LibraryApp.Application.Features.UserProfiles.ViewModels
{
    public class UserProfileViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string PhotoPath { get; set; }
    }
}

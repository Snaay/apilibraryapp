﻿using AutoMapper;
using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Features.Books.ViewModels.BookModel;
using LibraryApp.Application.Helpers;
using LibraryApp.Application.Wrappers;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.Books.Queries.GetBooks
{
    public class GetBooksQuery : IRequest<ListResponse<BookListViewModel>>
    {
        // По названию книги
        public string Name { get; set; }

        // По жанрам
        public int[] Subganres { get; set; }

        // По автору
        public int AuthorId { get; set; }

        // По издателю
        public int PublisherId { get; set; }

        public int CurrentPage { get; set; } = 1;
        public int PageSize { get; set; } = 10;
    }
    public class GetBooksQueryHandle : IRequestHandler<GetBooksQuery, ListResponse<BookListViewModel>>
    {
        private readonly ILibraryDbContext _context;
        private readonly IMapper _mapper;
        public GetBooksQueryHandle(ILibraryDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<ListResponse<BookListViewModel>> Handle(GetBooksQuery request, CancellationToken cancellationToken)
        {
            var response = new ListResponse<BookListViewModel>();

            int skipStart = HelperMethods.GetSkipStartPagination(request.CurrentPage, request.PageSize);

            var books = _context.Books
                            .Include(a => a.Author)
                                .Include(p => p.Publisher)
                                    .Include(g => g.Subganres)
                                    .ThenInclude(sg => sg.Subganre)
                                        .Include(c => c.Comments)
                                        .ThenInclude(a => a.User)
                                            .AsNoTracking();

            if (!string.IsNullOrEmpty(request.Name))
                books = books.Where(a => a.Name.ToLower().Contains(request.Name.ToLower()));

            if(request.AuthorId > 0)
                books = books.Where(a => a.AuthorId == request.AuthorId);

            if (request.PublisherId > 0)
                books = books.Where(a => a.PublisherId == request.PublisherId);

            if (request.Subganres != null)
                foreach(var subganre in request.Subganres)
                    books = books.Where(a => a.Subganres.Any(a => a.SubganreId == subganre));

            response.TotalItemsCount = await books.CountAsync(cancellationToken);

            books = books.OrderByDescending(a => a.DateAdded).Skip(skipStart).Take(request.PageSize);

            response.Items = _mapper.Map<List<BookListViewModel>>(books);

            return response;
        }
    }
}

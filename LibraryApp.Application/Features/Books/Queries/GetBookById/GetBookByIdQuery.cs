﻿using AutoMapper;
using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Features.Books.ViewModels.BookModel;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.Books.Queries.GetBookById
{
    public class GetBookByIdQuery : IRequest<BookDetailsViewModel>
    {
        public int BookId { get; set; }
    }
    public class GetBookByIdQueryHandle : IRequestHandler<GetBookByIdQuery, BookDetailsViewModel>
    {
        private readonly ILibraryDbContext _context;
        private readonly ICurrentUserService _currentUserService;
        private readonly IMapper _mapper;
        public GetBookByIdQueryHandle(ILibraryDbContext context, ICurrentUserService currentUserService, IMapper mapper)
        {
            _context = context;
            _currentUserService = currentUserService;
            _mapper = mapper;
        }
        public async Task<BookDetailsViewModel> Handle(GetBookByIdQuery request, CancellationToken cancellationToken)
        {
            var book = await _context.Books
                                .Include(a => a.Author)
                                    .Include(p => p.Publisher)
                                        .Include(b => b.Bookings.Where(a => a.UserId == _currentUserService.UserId))
                                            .Include(s => s.SubNotifications.Where(a => a.UserId == _currentUserService.UserId))
                                                .Include(g => g.Subganres)
                                                .ThenInclude(sg => sg.Subganre)
                                                    .Include(c => c.Comments.OrderByDescending(a => a.CommentDate))
                                                    .ThenInclude(u => u.User)
                                                    .ThenInclude(p => p.UserProfile)
                                                        .AsNoTracking()
                                                            .FirstOrDefaultAsync(b => b.Id == request.BookId);

            return _mapper.Map<BookDetailsViewModel>(book);
        }
    }
}

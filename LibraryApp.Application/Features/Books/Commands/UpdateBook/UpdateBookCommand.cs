﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Common.Interfaces.File;
using LibraryApp.Application.DTO.File;
using LibraryApp.Application.Exceptions;
using LibraryApp.Application.Wrappers;
using LibraryApp.Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.Books.Commands.UpdateBook
{
    public class UpdateBookCommand : IRequest<Response<int>>
    {
        public int BookId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public IFormFile Photo { get; set; }
        public int Year { get; set; }
        public int NumberPages { get; set; }
        public int Quantity { get; set; }

        public int AuthorId { get; set; }
        public int PublisherId { get; set; }
        public int[] Subganres { get; set; }
    }
    public class UpdateBookCommandHandler : IRequestHandler<UpdateBookCommand, Response<int>>
    {
        private readonly ILibraryDbContext _context;
        private readonly IFileService _fileService;
        public UpdateBookCommandHandler(ILibraryDbContext context, IFileService fileService)
        {
            _context = context;
            _fileService = fileService;
        }
        public async Task<Response<int>> Handle(UpdateBookCommand request, CancellationToken cancellationToken)
        {
            var book = await _context.Books
                        .Include(a => a.Author)
                            .Include(a => a.Publisher)
                                .Include(a => a.Subganres)
                                    .FirstOrDefaultAsync(a => a.Id == request.BookId, cancellationToken)
                                        ?? throw new ApiException("Книга не найден");

            var author = await _context.Authors
                .FirstOrDefaultAsync(a => a.Id == request.AuthorId, cancellationToken)
                    ?? throw new ApiException("Автор не найден");

            var publisher = await _context.Publishers
                .FirstOrDefaultAsync(a => a.Id == request.PublisherId, cancellationToken)
                    ?? throw new ApiException("Издатель не найден");

            var subganres = new List<GanreBook>();

            foreach (var subganreId in request.Subganres)
            {
                var subganre = await _context.Subganres
                    .FirstOrDefaultAsync(a => a.Id == subganreId, cancellationToken)
                        ?? throw new ApiException("Жанр(ы) не найден(ы)");

                subganres.Add(new GanreBook { SubganreId = subganreId });
            }

            if (request.Photo != null)
            {
                await _fileService.UpdateFile(new UpdateFileRequest
                {
                    FileStream = request.Photo,
                    FilePath = book.PhotoPath
                });
            }

            book.Name = request.Name;
            book.Description = request.Description;
            book.Year = request.Year;
            book.NumberPages = request.NumberPages;
            book.Quantity = request.Quantity;

            book.Author = author;
            book.Publisher = publisher;
            book.Subganres = subganres;

            await _context.SaveChangesAsync(cancellationToken);

            return new Response<int>(book.Id, "Книга обновлена");
        }
    }
}

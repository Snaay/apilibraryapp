﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Common.Interfaces.File;
using LibraryApp.Application.DTO.File;
using LibraryApp.Application.Enums;
using LibraryApp.Application.Exceptions;
using LibraryApp.Application.Wrappers;
using LibraryApp.Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.Books.Commands.CreateBook
{
    public class CreateBookCommand : IRequest<Response<int>>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public IFormFile Photo { get; set; }
        public int Year { get; set; }
        public int NumberPages { get; set; }
        public int Quantity { get; set; }

        public int AuthorId { get; set; }
        public int PublisherId { get; set; }
        public int[] Subganres { get; set; }
    }

    public class CreateBookCommandHandler : IRequestHandler<CreateBookCommand, Response<int>>
    {
        private readonly ILibraryDbContext _context;
        private readonly IFileService _fileService;
        public CreateBookCommandHandler(ILibraryDbContext context, IFileService fileService)
        { 
            _context = context;
            _fileService = fileService;
        }
        public async Task<Response<int>> Handle(CreateBookCommand request, CancellationToken cancellationToken)
        {
            var author = await _context.Authors
                .FirstOrDefaultAsync(a => a.Id == request.AuthorId, cancellationToken)
                    ?? throw new ApiException("Автор не найден");

            var publisher = await _context.Publishers
                .FirstOrDefaultAsync(a => a.Id == request.PublisherId, cancellationToken)
                    ?? throw new ApiException("Издатель не найден");

            var subganres = new List<GanreBook>();

            foreach (var subganreId in request.Subganres)
            {
                var subganre = await _context.Subganres
                    .FirstOrDefaultAsync(a => a.Id == subganreId, cancellationToken)
                        ?? throw new ApiException("Жанр(ы) не найден(ы)");
                subganres.Add(new GanreBook { SubganreId = subganreId });
            }

            var createFileRequest = new CreateFileRequest
            {
                FileStream = request.Photo,
                FolderTypeId = FolderType.Book
            };

            var filePathResult = await _fileService.CreateFile(createFileRequest);

            var book = await _context.Books.AddAsync(new Book
            {
                Name = request.Name,
                Description = request.Description,
                Year = request.Year,
                NumberPages = request.NumberPages,
                Quantity = request.Quantity,
                PhotoPath = filePathResult,
                AuthorId = request.AuthorId,
                PublisherId = request.PublisherId,
                DateAdded = DateTime.Now,
                Subganres = subganres
            });

            await _context.SaveChangesAsync(cancellationToken);

            return new Response<int>(book.Entity.Id, "Книга добавлена");
        }
    }
}

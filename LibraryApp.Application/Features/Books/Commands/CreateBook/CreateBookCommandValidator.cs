﻿using FluentValidation;
using LibraryApp.Application.Features.Books.Commands.CreateBook;
using LibraryApp.Application.Features.Validators;

namespace LibraryApp.Application.Features.Books.Validators
{
    public class CreateBookCommandValidator : AbstractValidator<CreateBookCommand>
    {
        public CreateBookCommandValidator()
        {
            RuleFor(v => v.Name).NotEmpty().WithMessage("Введите название книги");
            RuleFor(v => v.Description).NotEmpty().WithMessage("Введите описание книги");
            RuleFor(v => v.Photo).NotNull().Photo();
            RuleFor(v => v.Year).NotEmpty().WithMessage("Введите год книги");
            RuleFor(v => v.NumberPages).NotEmpty().WithMessage("Введите кол-во страниц книги");
            RuleFor(v => v.Quantity).NotEmpty().WithMessage("Введите кол-во экземпляров книги");
            RuleFor(v => v.AuthorId).NotEmpty().WithMessage("Укажите автора книги");
            RuleFor(v => v.PublisherId).NotEmpty().WithMessage("Укажите издателя книги");
            RuleFor(v => v.Subganres).NotNull().WithMessage("Укажите жанры книги");
        }
    }
}

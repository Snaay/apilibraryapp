﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Common.Interfaces.File;
using LibraryApp.Application.DTO.File;
using LibraryApp.Application.Exceptions;
using LibraryApp.Application.Wrappers;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.Books.Commands.DeleteBook
{
    public class DeleteBookCommand : IRequest<Response<int>>
    {
        public int BookId { get; set; }
    }
    public class DeleteBookCommandHandler : IRequestHandler<DeleteBookCommand, Response<int>>
    {
        private readonly ILibraryDbContext _context;
        private readonly IFileService _fileService;
        public DeleteBookCommandHandler(ILibraryDbContext context, IFileService fileService)
        {
            _context = context;
            _fileService = fileService;
        }
        public async Task<Response<int>> Handle(DeleteBookCommand request, CancellationToken cancellationToken)
        {
            var book = await _context.Books
                .FirstOrDefaultAsync(b => b.Id == request.BookId, cancellationToken)
                    ?? throw new ApiException("Книга не найдена");

            var fileRequest = new DeleteFileRequest
            {
                FilePath = book.PhotoPath
            };

            await _fileService.DeleteFile(fileRequest);

            _context.Books.Remove(book);

            await _context.SaveChangesAsync(cancellationToken);

            return new Response<int>(book.Id, "Книга удалена");
        }
    }
}

﻿using FluentValidation;

namespace LibraryApp.Application.Features.Books.Commands.DeleteBook
{
    public class DeleteBookCommandValidator : AbstractValidator<DeleteBookCommand>
    {
        public DeleteBookCommandValidator()
        {
            RuleFor(x => x.BookId).NotEmpty().WithMessage("Id книги отсутствует");
        }
    }
}

﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Enums;
using LibraryApp.Application.Features.Notifications.Commands.CreateNotification;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.Books.Commands.SetQuantityBook
{
    public class SetQuantityBookCommand : INotification
    {
        public int BookId { get; set; }
        public SetQuantityType QuantityType { get; set; }
    }
    public class SetQuantityBookCommandHandler : INotificationHandler<SetQuantityBookCommand>
    {
        private readonly ILibraryDbContext _context;
        private readonly IMediator _mediator;
        public SetQuantityBookCommandHandler(ILibraryDbContext context, IMediator mediator)
        {
            _context = context;
            _mediator = mediator;
        }
        public async Task Handle(SetQuantityBookCommand request, CancellationToken cancellationToken)
        {
            var book = await _context.Books.FirstOrDefaultAsync(a => a.Id == request.BookId);

            // если кол-во книг осталось 1, и ее уменьшают (т.е кол-во будет 0), 
            // то удаляем уведомление о доступности бронирования этой книги
            // и всех подписчиков из этого уведомления - кто не успел забронировать после уведомления
            if (book.Quantity == 1 && request.QuantityType == SetQuantityType.Dec) 
            {
                var notification = await _context.Notifications.FirstOrDefaultAsync(a => a.BookId == book.Id);

                if (notification != null)
                {
                    var receiveNotifications = _context.ReceiveNotifications.Where(a => a.NotificationId == notification.Id);

                    _context.ReceiveNotifications.RemoveRange(receiveNotifications);

                    _context.Notifications.Remove(notification);
                }
            }
            // если кол-во книг было 0, и книгу убирают из брони (т.е кол-во будет 1),
            // то уведомляем подписчиков что книга доступна для бронирования
            if (book.Quantity == 0 && request.QuantityType == SetQuantityType.Inc) 
            {
                await _mediator.Publish(new CreateNotificationCommand { BookId = book.Id }, cancellationToken);
            }

            switch (request.QuantityType)
            {
                case SetQuantityType.Inc:
                    book.Quantity += 1;
                    break;
                case SetQuantityType.Dec:
                    book.Quantity -= 1;
                    break;
            }

            await _context.SaveChangesAsync(cancellationToken);

            await Task.CompletedTask;
        }
    }
}

﻿namespace LibraryApp.Application.Features.Books.ViewModels
{
    public class AuthorBookViewModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
    }
}

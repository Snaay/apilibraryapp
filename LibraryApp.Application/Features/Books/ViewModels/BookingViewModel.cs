﻿namespace LibraryApp.Application.Features.Books.ViewModels
{
    public class BookingViewModel
    {
        public int Id { get; set; }
        public bool IsGiven { get; set; }
    }
}

﻿namespace LibraryApp.Application.Features.Books.ViewModels
{
    public class PublisherBookViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}

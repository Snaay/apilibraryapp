﻿using LibraryApp.Domain.Enums;
using System;

namespace LibraryApp.Application.Features.Books.ViewModels
{
    public class CommentBookViewModel
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string CommentDate { get; set; }
        public int RatingTypeId { get; set; }

        public UserViewModel User { get; set; }
    }
}

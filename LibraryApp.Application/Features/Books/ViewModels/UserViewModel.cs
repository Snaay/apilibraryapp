﻿namespace LibraryApp.Application.Features.Books.ViewModels
{
    public class UserViewModel
    {
        public string UserName { get; set; }
        public string PhotoPath { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
    }
}

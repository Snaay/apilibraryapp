﻿using LibraryApp.Application.Features.Subganres.ViewModels;
using System.Collections.Generic;

namespace LibraryApp.Application.Features.Books.ViewModels.BookModel
{
    public class BookListViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string PhotoPath { get; set; }
        public int Year { get; set; }
        public int NumberPages { get; set; }
        public int Quantity { get; set; }
        public AuthorBookViewModel Author { get; set; }
        public PublisherBookViewModel Publisher { get; set; }
        public List<SubganreViewModel> Subganres { get; set; }
        public int LikesCount { get; set; }
        public int DislikesCount { get; set; }
        public int CommentCount { get; set; }
    }
}

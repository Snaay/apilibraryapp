﻿using FluentValidation;
using Microsoft.AspNetCore.Http;

namespace LibraryApp.Application.Features.Validators
{
    public static class ValidatorExtensions
    {
        public static IRuleBuilder<T, string> Password<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            var options = ruleBuilder
                .NotEmpty().WithMessage("Пароль обязательный для заполнения")
                .MinimumLength(6).WithMessage("Пароль должен состоять не менее чем из 6 символов");

            return options;
        }
        public static IRuleBuilder<T, string> Login<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            var options = ruleBuilder
                .NotEmpty().WithMessage("Логин обязательный для заполнения")
                .MinimumLength(5).WithMessage("Логин должен состоять не менее чем из 5 символов");
            
            return options;
        }
        public static IRuleBuilder<T, string> Email<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            var options = ruleBuilder
                .NotEmpty().WithMessage("Email обязательный для заполнения")
                .EmailAddress().WithMessage("Email не корректный");

            return options;
        }
        public static IRuleBuilder<T, IFormFile> Photo<T>(this IRuleBuilder<T, IFormFile> ruleBuilder)
        {
            var options = ruleBuilder.NotNull().WithMessage("Добавте фото");

            return options;
        }
    }
}

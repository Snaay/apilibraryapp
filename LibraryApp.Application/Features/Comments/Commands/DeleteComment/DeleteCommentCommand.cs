﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Exceptions;
using LibraryApp.Application.Wrappers;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.Comments.Commands.DeleteComment
{
    public class DeleteCommentCommand : IRequest<Response<int>>
    {
        public int CommentId { get; set; }
    }
    public class DeleteCommentCommandHandler : IRequestHandler<DeleteCommentCommand, Response<int>>
    {
        private readonly ILibraryDbContext _context;
        private readonly ICurrentUserService _currentUserService;
        public DeleteCommentCommandHandler(ILibraryDbContext context, ICurrentUserService currentUserService)
        {
            _context = context;
            _currentUserService = currentUserService;
        }
        public async Task<Response<int>> Handle(DeleteCommentCommand request, CancellationToken cancellationToken)
        {
            var comment = await _context.Comments.FirstOrDefaultAsync(c => c.Id == request.CommentId)
                ?? throw new ApiException("Коментарий не найден");

            _context.Comments.Remove(comment);

            await _context.SaveChangesAsync(cancellationToken);

            return new Response<int>(comment.Id, "Коментарий удален");
        }
    }
}

﻿using FluentValidation;

namespace LibraryApp.Application.Features.Comments.Commands.DeleteComment
{
    class DeleteCommentValidator : AbstractValidator<DeleteCommentCommand>
    {
        public DeleteCommentValidator()
        {
            RuleFor(a => a.CommentId).NotNull().WithMessage("Id комментария отсутствует");
        }
    }
}

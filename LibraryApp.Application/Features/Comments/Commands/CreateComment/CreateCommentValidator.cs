﻿using FluentValidation;

namespace LibraryApp.Application.Features.Comments.Commands.CreateComment
{
    public class CreateCommentValidator : AbstractValidator<CreateCommentCommand>
    {
        public CreateCommentValidator()
        {
            RuleFor(a => a.CommentText).NotNull().WithMessage("Комментарий отсутствует");
            RuleFor(a => a.RatingTypeId).NotNull().WithMessage("Оценка книги отсутствует");
            RuleFor(a => a.BookId).NotNull().WithMessage("Id книги отсутствует");
        }
    }
}

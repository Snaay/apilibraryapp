﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Exceptions;
using LibraryApp.Application.Wrappers;
using LibraryApp.Domain.Entities;
using LibraryApp.Domain.Enums;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.Application.Features.Comments.Commands.CreateComment
{
    public class CreateCommentCommand : IRequest<Response<int>>
    {
        public string CommentText { get; set; }
        public int RatingTypeId { get; set; }
        public int BookId { get; set; }
    }
    public class CreateCommentCommandHandler : IRequestHandler<CreateCommentCommand, Response<int>>
    {
        private readonly ILibraryDbContext _context;
        private readonly ICurrentUserService _currentUserService;
        public CreateCommentCommandHandler(ILibraryDbContext context, ICurrentUserService currentUserService)
        {
            _context = context;
            _currentUserService = currentUserService;
        }
        public async Task<Response<int>> Handle(CreateCommentCommand request, CancellationToken cancellationToken)
        {
            var book = await _context.Books.FirstOrDefaultAsync(a => a.Id == request.BookId)
                ?? throw new ApiException("Книга не найдена");

            var comment = await _context.Comments.FirstOrDefaultAsync(a => a.UserId == _currentUserService.UserId && a.BookId == book.Id);

            if (comment != null)
                throw new ApiException("Вы уже оставили отзыв по данной книге");

            comment = new Comment
            {
                Text = request.CommentText,
                RatingTypeId = (RatingType)request.RatingTypeId,
                UserId = _currentUserService.UserId,
                Book = book,
                CommentDate = DateTime.UtcNow
            };

            await _context.Comments.AddAsync(comment);

            await _context.SaveChangesAsync(cancellationToken);

            return new Response<int>(comment.Id, "Комментарий добавлен");
        }
    }
}

﻿namespace LibraryApp.Domain.Entities.User
{
    public class UserProfile
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string PhotoPath { get; set; }

        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
    }
}

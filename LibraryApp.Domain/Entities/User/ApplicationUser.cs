﻿using LibraryApp.Domain.Entities.Notification;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LibraryApp.Domain.Entities.User
{
    public class ApplicationUser : IdentityUser
    {
        public UserProfile UserProfile { get; set; }
        public DateTime DateCreated { get; set; }

        public List<Comment> Comments { get; set; }
        public List<Booking> Bookings { get; set; }
        public List<SubNotification> SubNotifications { get; set; }
        public List<ReceiveNotification> ReceiveNotifications { get; set; }
    }
}

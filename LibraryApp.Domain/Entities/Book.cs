﻿using LibraryApp.Domain.Entities.Notification;
using System;
using System.Collections.Generic;

namespace LibraryApp.Domain.Entities
{
    public class Book
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string PhotoPath { get; set; }
        public int Year { get; set; }
        public int NumberPages { get; set; }
        public int Quantity { get; set; }
        public DateTime DateAdded { get; set; }

        public int PublisherId { get; set; }
        public Publisher Publisher { get; set; }

        public int AuthorId { get; set; }
        public Author Author { get; set; }

        public List<GanreBook> Subganres { get; set; } // жанры

        public List<Comment> Comments { get; set; } // комментарии

        public List<Booking> Bookings { get; set; } // брони

        public List<SubNotification> SubNotifications { get; set; } // подписанны на уведомление, когда освободится книга

        public Notification.Notification Notification { get; set; } // уведомление по этой книги
    }
}

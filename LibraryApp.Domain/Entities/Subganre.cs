﻿using System.Collections.Generic;

namespace LibraryApp.Domain.Entities
{
    public class Subganre
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int GanreId { get; set; }
        public Ganre Ganre { get; set; }

        public List<GanreBook> GanreBooks { get; set; }
    }
}

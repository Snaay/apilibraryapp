﻿using LibraryApp.Domain.Entities.User;
using System;

namespace LibraryApp.Domain.Entities
{
    public class Booking
    {
        public int Id { get; set; }

        public int BookId { get; set; }
        public Book Book { get; set; }

        public string UserId { get; set; }
        public ApplicationUser User { get; set; }

        public bool IsGiven { get; set; }

        public DateTime BookingDate { get; set; }
    }
}

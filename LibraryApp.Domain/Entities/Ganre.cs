﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryApp.Domain.Entities
{
    // жанр
    public class Ganre
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public List<Subganre> Subganres { get; set; }
    }
}

﻿namespace LibraryApp.Domain.Entities
{
    public class GanreBook
    {
        public int Id { get; set; }

        public int BookId { get; set; }
        public Book Book { get; set; }

        public int SubganreId { get; set; }
        public Subganre Subganre { get; set; }
    }
}

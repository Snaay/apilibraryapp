﻿using LibraryApp.Domain.Entities.User;
using LibraryApp.Domain.Enums;
using System;

namespace LibraryApp.Domain.Entities
{
    public class Comment
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public DateTime CommentDate { get; set; }

        public RatingType RatingTypeId { get; set; }

        public string UserId { get; set; }
        public ApplicationUser User { get; set; }

        public int BookId { get; set; }
        public Book Book { get; set; }
    }
}

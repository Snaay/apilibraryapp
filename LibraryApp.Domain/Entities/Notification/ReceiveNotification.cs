﻿using LibraryApp.Domain.Entities.User;

namespace LibraryApp.Domain.Entities.Notification
{
    public class ReceiveNotification
    {
        public int Id { get; set; }

        public bool IsRead { get; set; }

        public int NotificationId { get; set; }
        public Notification Notification { get; set; }

        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
    }
}

﻿using System;

namespace LibraryApp.Domain.Entities.Notification
{
    public class Notification
    {
        public int Id { get; set; }

        public int BookId { get; set; }
        public Book Book { get; set; }

        public DateTime ArrivalDate { get; set; }
    }
}

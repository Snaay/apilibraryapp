﻿using LibraryApp.Domain.Entities.User;
using LibraryApp.Domain.Enums;

namespace LibraryApp.Domain.Entities.Notification
{
    public class SubNotification 
    {
        public int Id { get; set; }

        // куда будут приходить уведомления
        public SubNotificationType SubNotificationTypeId { get; set; }

        // книга на которую подписался пользователь
        public int BookId { get; set; }
        public Book Book { get; set; }

        // пользователь который подписался н уведомление
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
    }
}

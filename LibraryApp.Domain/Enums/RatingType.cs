﻿namespace LibraryApp.Domain.Enums
{
    public enum RatingType : int
    {
        Like = 0,
        Dislike = 1
    }
}

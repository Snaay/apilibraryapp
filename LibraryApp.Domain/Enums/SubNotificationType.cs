﻿namespace LibraryApp.Domain.Enums
{
    // тип подписки, куда будет приходить уведомление
    public enum SubNotificationType : int
    {
        Service = 0, // на сервис
        Email = 1 // на почту
    }
}

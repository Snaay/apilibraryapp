﻿namespace LibraryApp.Domain.Settings
{
    public class BookingSettings
    {
        public int MaxNumberBookings { get; set; }
        public int LimitedBookingTimeMinutes { get; set; }
    }
}

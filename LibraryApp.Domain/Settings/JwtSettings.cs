﻿namespace LibraryApp.Domain.Settings
{
    public class JwtSettings
    {
        public string Key { get; set; } // секретный ключ для шифрации
        public string Issuer { get; set; } // издатель токена
        public string Audience { get; set; } // потребитель токена
        public int LifeTime { get; set; } // время жизни токена
    }
}

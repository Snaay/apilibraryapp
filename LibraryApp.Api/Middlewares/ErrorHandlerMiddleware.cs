﻿using LibraryApp.Application.Exceptions;
using LibraryApp.Application.Wrappers;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Net;
using System.Threading.Tasks;

namespace LibraryApp.Api.Middlewares
{
    public class ErrorHandlerMiddleware
    {
        private readonly RequestDelegate _next;
        public ErrorHandlerMiddleware(RequestDelegate next) => _next = next;
        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch(Exception exception)
            {
                var response = context.Response;

                response.ContentType = "application/json";

                response.StatusCode = (int)HttpStatusCode.OK;

                var result = new Response<string>()
                {
                    Succeeded = false
                };

                if (exception is ApiValidationException apiValidationException)
                {
                    result.Errors = apiValidationException?.Errors;
                    result.Message = apiValidationException?.Message;
                }
                else if (exception is ApiException apiException)
                {
                    result.Message = apiException?.Message;
                }
                else
                {
                    result.Message = "Ошибка на сервере";
                }

                var serializerSettings = new JsonSerializerSettings();

                serializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

                await response.WriteAsync(JsonConvert.SerializeObject(result, serializerSettings));
            }
        }
    }
}

using System;
using System.Threading.Tasks;
using LibraryApp.Api.Extensions;
using LibraryApp.Application.Quartz;
using LibraryApp.Domain.Entities.User;
using LibraryApp.Infrastructure.Seeds;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Quartz;

namespace LibraryApp.Api
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();

            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                try
                {
                    var userManager = services.GetRequiredService<UserManager<ApplicationUser>>();
                    var roleManager = services.GetRequiredService<RoleManager<IdentityRole>>();
                    var configuration = services.GetRequiredService<IConfiguration>();

                    await DefaultRoles.CreateDefaultRolesAsync(roleManager);
                    await DefaultAdmin.CreateDefaultAdmin(userManager, configuration);
                    await DefaultLibrarian.CreateDefaultLibrarian(userManager);
                    await DefaultCustomer.CreateDefaultCustomer(userManager);
                }
                catch(Exception ex)
                {
                    var logger = services.GetRequiredService<ILogger<Program>>();
                    logger.LogError(ex, "��������� ������ ��� ���������� ���� ������");
                }
            }

            await host.RunAsync();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddQuartz(q =>
                    {
                        q.UseMicrosoftDependencyInjectionScopedJobFactory();

                        q.AddJobAndTrigger<AutoDeleteExpiredBookingsJob>(hostContext.Configuration);
                    });
                    services.AddQuartzHostedService(q => q.WaitForJobsToComplete = true);
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}

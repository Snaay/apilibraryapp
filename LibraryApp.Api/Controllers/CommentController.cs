﻿using LibraryApp.Api.Common;
using LibraryApp.Application.Features.Comments.Commands.CreateComment;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LibraryApp.Api.Controllers
{
    public class CommentController : CommonApiController
    {
        [HttpPost]
        public async Task<IActionResult> CreateComment(CreateCommentCommand request)
        {
            return Ok(await Mediator.Send(request));
        }
    }
}

﻿using LibraryApp.Api.Common;
using LibraryApp.Application.Features.Ganres.Queries.GetGanres;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LibraryApp.Api.Controllers
{
    public class GanreController : CommonApiController
    {
        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetGanres()
        {
            return Ok(await Mediator.Send(new GetGanresQuery()));
        }
    }
}

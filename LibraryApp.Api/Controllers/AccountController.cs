﻿using LibraryApp.Api.Common;
using LibraryApp.Application.Features.Auth.Commands.AuthMe;
using LibraryApp.Application.Features.Auth.Commands.ChangePassword;
using LibraryApp.Application.Features.Auth.Commands.ConfirmEmail;
using LibraryApp.Application.Features.Auth.Commands.ForgotPassword;
using LibraryApp.Application.Features.Auth.Commands.Login;
using LibraryApp.Application.Features.Auth.Commands.Register;
using LibraryApp.Application.Features.Auth.Commands.ResetPassword;
using LibraryApp.Domain.Settings;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;

namespace LibraryApp.Api.Controllers
{
    [Authorize]
    public class AccountController : CommonApiController
    {
        private readonly JwtSettings _jwtSetting;
        public AccountController(IOptions<JwtSettings> jwtSetting)
        {
            _jwtSetting = jwtSetting.Value;
        }
        [AllowAnonymous]
        [HttpPost("auth-me")]
        public async Task<IActionResult> AuthMe()
        {
            return Ok(await Mediator.Send(new AuthMeCommand()));
        }
        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<IActionResult> AuthenticateAsync(LoginCommand request)
        {
            var result = await Mediator.Send(request);

            // С помощью cookie policy эти куки автоматически отправятся как httpOnly и secure
            HttpContext.Response.Cookies.Append("Application.Id", result.Data.Token, 
                new CookieOptions { MaxAge = TimeSpan.FromMinutes(_jwtSetting.LifeTime) });

            return Ok(result);
        }
        [AllowAnonymous]
        [HttpPost("register")]
        public async Task<IActionResult> RegisterAsync(RegisterCommand request)
        {
            request.Origin = HttpContext.Request.Headers["origin"];

            return Ok(await Mediator.Send(request));
        }
        [AllowAnonymous]
        [HttpPost("confirm-email")]
        public async Task<IActionResult> ConfirmEmailAsync(ConfirmEmailCommand request)
        {
            return Ok(await Mediator.Send(request));
        }
        [AllowAnonymous]
        [HttpPost("forgot-password")]
        public async Task<IActionResult> ForgotPasswordAsync(ForgotPasswordCommand request)
        {
            request.Origin = HttpContext.Request.Headers["origin"];

            return Ok(await Mediator.Send(request));
        }
        [AllowAnonymous]
        [HttpPost("reset-password")]
        public async Task<IActionResult> ResetPasswordAsync(ResetPasswordCommand request)
        {
            return Ok(await Mediator.Send(request));
        }
        [HttpPost("change-password")]
        public async Task<IActionResult> ChangePasswordAsync(ChangePasswordCommand request)
        {
            return Ok(await Mediator.Send(request));
        }
        [HttpPost("logout")]
        public async Task<IActionResult> Logout()
        {
            HttpContext.Response.Cookies.Append("Application.Id", "",
                    new CookieOptions { MaxAge = TimeSpan.FromSeconds(0) });

            return Ok();
        }
    }
}

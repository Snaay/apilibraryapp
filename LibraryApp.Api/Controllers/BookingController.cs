﻿using LibraryApp.Api.Common;
using LibraryApp.Application.Features.Bookings.Commands.CreateBooking;
using LibraryApp.Application.Features.Bookings.Commands.DeleteBooking;
using LibraryApp.Application.Features.Bookings.Queries.GetUserBookings;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LibraryApp.Api.Controllers
{
    public class BookingController : CommonApiController
    {
        [HttpGet] // api/booking
        public async Task<IActionResult> GetBookings() // брони текущего пользователя пользователя
        {
            return Ok(await Mediator.Send(new GetUserBookingsQuery()));
        }
        [HttpPost] // api/booking
        public async Task<IActionResult> BookingBook(CreateBookingCommand request) // создать бронь
        {
            return Ok(await Mediator.Send(request));
        }
        [HttpDelete("{bookingId}")] // api/booking/{bookingId}
        public async Task<IActionResult> DeleteBooking([FromRoute]DeleteBookingCommand request) // удалить бронь
        {
            return Ok(await Mediator.Send(request));
        }
    }
}

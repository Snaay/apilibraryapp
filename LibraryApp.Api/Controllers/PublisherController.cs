﻿using LibraryApp.Api.Common;
using LibraryApp.Application.Features.Publishers.Queries.GetPublishers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LibraryApp.Api.Controllers
{
    public class PublisherController : CommonApiController
    {
        [AllowAnonymous]
        [HttpPost("search")]
        public async Task<IActionResult> GetPublishers(GetPublishersQuery request)
        {
            return Ok(await Mediator.Send(request));
        }
    }
}

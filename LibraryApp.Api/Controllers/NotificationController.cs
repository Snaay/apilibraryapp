﻿using LibraryApp.Api.Common;
using LibraryApp.Application.Features.Notifications.Commands.CheckNotfications;
using LibraryApp.Application.Features.Notifications.Commands.ReadNotifications;
using LibraryApp.Application.Features.Notifications.Queries.GetNotification;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LibraryApp.Api.Controllers
{
    public class NotificationController : CommonApiController
    {
        [HttpGet]
        public async Task<IActionResult> GetNotifications()
        {
            return Ok(await Mediator.Send(new GetNotificationsQuery()));
        }
        [HttpGet("read")]
        public async Task<IActionResult> ReadNotifications()
        {
            return Ok(await Mediator.Send(new ReadNotificationsCommand()));
        }
        [HttpGet("check")]
        public async Task<IActionResult> CheckNotifications()
        {
            return Ok(await Mediator.Send(new CheckNotificationCommand()));
        }
    }
}

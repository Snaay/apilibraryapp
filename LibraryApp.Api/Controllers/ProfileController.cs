﻿using LibraryApp.Api.Common;
using LibraryApp.Application.Features.UserProfiles.Commands.UpdateUserProfile;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LibraryApp.Api.Controllers
{
    public class ProfileController : CommonApiController
    {
        [HttpPost]
        public async Task<IActionResult> UpdateProfile(UpdateUserProfileCommand request)
        {
            return Ok(await Mediator.Send(request));
        }
    }
}

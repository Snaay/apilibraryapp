﻿using LibraryApp.Api.Common;
using LibraryApp.Application.Features.SubNotifications.Commands.CreateSubNotification;
using LibraryApp.Application.Features.SubNotifications.Commands.DeleteSubNotification;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LibraryApp.Api.Controllers
{
    public class SubNotificationController : CommonApiController
    {
        [HttpPost]
        public async Task<IActionResult> CreateSubNotification(CreateSubNotificationCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
        [HttpDelete("{bookId}")]
        public async Task<IActionResult> UnSubNotification([FromRoute]DeleteSubNotificationCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
    }
}

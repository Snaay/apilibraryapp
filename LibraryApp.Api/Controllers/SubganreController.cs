﻿using LibraryApp.Api.Common;
using LibraryApp.Application.Features.Subganres.Queries.GetSubganreByGanreId;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LibraryApp.Api.Controllers
{
    public class SubganreController : CommonApiController
    {
        [AllowAnonymous]
        [HttpGet("{ganreId}")]
        public async Task<IActionResult> GetSubganres([FromRoute]GetSubganreByGanreIdQuery request)
        {
            return Ok(await Mediator.Send(request));
        }
    }
}

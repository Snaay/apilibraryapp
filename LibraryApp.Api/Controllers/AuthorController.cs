﻿using LibraryApp.Api.Common;
using LibraryApp.Application.Features.Authors.Queries.FilterAuthorSelection;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LibraryApp.Api.Controllers
{
    public class AuthorController : CommonApiController
    {
        [AllowAnonymous]
        [HttpPost("search")]
        public async Task<IActionResult> GetAuthors(GetAuthorsQuery request)
        {
            return Ok(await Mediator.Send(request));
        }
    }
}

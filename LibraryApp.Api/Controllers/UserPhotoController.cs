﻿using LibraryApp.Api.Common;
using LibraryApp.Application.Features.UserPhotoProfile.Commands.CreateUserPhoto;
using LibraryApp.Application.Features.UserPhotoProfile.Commands.DeleteUserPhoto;
using LibraryApp.Application.Features.UserPhotoProfile.Commands.UpdateUserPhoto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LibraryApp.Api.Controllers
{
    public class UserPhotoController : CommonApiController
    {
        [HttpPost("create")]
        public async Task<IActionResult> CreateUserPhoto([FromForm]CreateUserPhotoCommand request)
        {
            return Ok(await Mediator.Send(request));
        }
        [HttpPost("update")]
        public async Task<IActionResult> UpdateUserPhoto([FromForm] UpdateUserPhotoCommand request)
        {
            return Ok(await Mediator.Send(request));
        }
        [HttpDelete]
        public async Task<IActionResult> DeleteUserPhoto()
        {
            return Ok(await Mediator.Send(new DeleteUserPhotoCommand()));
        }
    }
}

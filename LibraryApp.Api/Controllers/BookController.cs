﻿using LibraryApp.Application.Features.Books.Queries.GetBooks;
using LibraryApp.Application.Features.Books.Queries.GetBookById;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using LibraryApp.Api.Common;
using Microsoft.AspNetCore.Authorization;

namespace LibraryApp.Api.Controllers
{
    public class BookController : CommonApiController
    {
        [AllowAnonymous]
        [HttpPost("search")]
        public async Task<IActionResult> GetBooks(GetBooksQuery request)
        {
            return Ok(await Mediator.Send(request));
        }
        [AllowAnonymous]
        [HttpGet("{bookId}")]
        public async Task<IActionResult> GetBook([FromRoute]GetBookByIdQuery request)
        {
            return Ok(await Mediator.Send(request));
        }
    }
}

using LibraryApp.Api.Middlewares;
using LibraryApp.Api.Services;
using LibraryApp.Application;
using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Infrastructure;
using LibraryApp.Infrastructure.Services;
using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.CookiePolicy;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace LibraryApp.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; set; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddApplication();
            services.AddInfrastructure(Configuration);

            services.AddServices(Configuration);

            services.AddScoped<ICurrentUserService, CurrentUserService>();

            services.AddCors();

            services.AddMvc();
        }
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }
            // ����������� cookie-policy �� httpOnly � secure
            app.UseCookiePolicy(new CookiePolicyOptions
            {
                MinimumSameSitePolicy = SameSiteMode.None,
                HttpOnly = HttpOnlyPolicy.Always,
                Secure = CookieSecurePolicy.Always,
            });

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseRouting();

            app.UseCors(c => c.WithOrigins("http://localhost:3000") // ��� SPA (React)
                                    .AllowCredentials()
                                        .AllowAnyMethod()
                                            .AllowAnyHeader());

            app.Use(async (context, next) => // jwt �� ��� � ��������� �������
            {
                string token = context.Request.Cookies["Application.Id"]; // ����� ����� �� ���
                if (!string.IsNullOrEmpty(token))
                    context.Request.Headers.Add("Authorization", "Bearer " + token); // ������ ���� � ��������� �������
                // ��������� X-Content-Type-Options ������������ ��� ������ �� ����������� ���� MIME sniffing.
                // ��� ���������� ����� ����������, ����� ���� ��������� ������������� ��������� �������,
                // ������ ������������ ��������� ������������ ��� ����� ��� ���-�� ������.
                // ��� ����� ���� ��������������� ����������� ��������� cross-site scripting �������� ��� ���������������� ���-����.
                context.Response.Headers.Add("X-Content-Type-Options", "nosniff");
                // ��� ����������� �������� ����� ���������� ����������� ���������� XSS,
                // ������� �������� ������� ���������� XSS �� ����, ��� �������� ����� ��������� ���������� ���. 
                // �� ��������� ��� �������� � ��������, �� ������������ ����� ��������� ������ � ��������� ��.
                // ��������� ��������� X-XSS-Protection, �� ����� ���������� ������� �������� ������������ ��, 
                // ��� ������ ������������, � ��������� ���������� ������.
                context.Response.Headers.Add("X-Xss-Protection", "1");
                // X-Frame-Options �������� ��������, ��� ���� ��� ���� ������� ������ HTML-������, �� ������ �� ����������.
                // ��� ����� ����� ��� ������� �������� ���� �� ������� clickjacking-������.
                context.Response.Headers.Add("X-Frame-Options", "DENY"); // � ����������� �� ������ ������ (nodejs)

                await next();
            });

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseMiddleware<ErrorHandlerMiddleware>();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

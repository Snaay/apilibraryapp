﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace LibraryApp.Api.Common
{
    [ApiController]
    public abstract class BaseApiController : Controller
    {
        private IMediator _mediator;
        protected IMediator Mediator => _mediator ??= HttpContext.RequestServices.GetService<IMediator>();
    }

    [Authorize]
    [Route("api/[controller]")] // общий
    public class CommonApiController : BaseApiController { }

    [Authorize(Roles = "Admin")]
    [Route("api/admin/[controller]")] // для админа
    public class AdminApiController : BaseApiController { }

    [Authorize(Roles = "Librarian")]
    [Route("api/librarian/[controller]")] // для библиотекаря или админа
    public class LibrarianApiController : BaseApiController { }
}

﻿using LibraryApp.Api.Common;
using LibraryApp.Application.Features.User.Commands.CreateUser;
using LibraryApp.Application.Features.User.Commands.DeleteUser;
using LibraryApp.Application.Features.User.Commands.SetPassword;
using LibraryApp.Application.Features.User.Queries.GetUsers;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LibraryApp.Api.Areas.Admin.Controllers
{
    public class UserController : AdminApiController
    {
        [HttpPost("search")]
        public async Task<IActionResult> GetUsers(GetUsersQuery request)
        {
            return Ok(await Mediator.Send(request));
        }
        [HttpPost]
        public async Task<IActionResult> CreateUser(CreateUserCommand request)
        {
            return Ok(await Mediator.Send(request));
        }
        [HttpDelete("{userId}")]
        public async Task<IActionResult> DeleteUser([FromRoute] DeleteUserCommand request)
        {
            return Ok(await Mediator.Send(request));
        }
        [HttpPost("set-password")]
        public async Task<IActionResult> SetUserPassword(SetPasswordCommand request)
        {
            return Ok(await Mediator.Send(request));
        }
    }
}

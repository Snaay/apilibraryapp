﻿using LibraryApp.Api.Common;
using LibraryApp.Application.Features.Authors.Commands.CreateAuthor;
using LibraryApp.Application.Features.Authors.Commands.DeleteAuthor;
using LibraryApp.Application.Features.Authors.Commands.UpdateAuthor;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LibraryApp.Api.Areas.Librarian.Controllers
{
    public class AuthorController : LibrarianApiController
    {
        [HttpPost]
        public async Task<IActionResult> CreateAuthor(CreateAuthorCommand request)
        {
            return Ok(await Mediator.Send(request));
        }
        [HttpPut]
        public async Task<IActionResult> UpdateAuthor(UpdateAuthorCommand request)
        {
            return Ok(await Mediator.Send(request));
        }
        [HttpDelete("{authorId}")]
        public async Task<IActionResult> DeleteAuthor([FromRoute]DeleteAuthorCommand request)
        {
            return Ok(await Mediator.Send(request));
        }
    }
}

﻿using LibraryApp.Api.Common;
using LibraryApp.Application.Features.Ganres.Commands.CreateGanre;
using LibraryApp.Application.Features.Ganres.Commands.DeleteGanre;
using LibraryApp.Application.Features.Ganres.Commands.UpdateGanre;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LibraryApp.Api.Areas.Librarian.Controllers
{
    public class GanreController : LibrarianApiController
    {
        [HttpPost]
        public async Task<IActionResult> CreateGanre(CreateGanreCommand request)
        {
            return Ok(await Mediator.Send(request));
        }
        [HttpPut]
        public async Task<IActionResult> UpdateGanre(UpdateGanreCommand request)
        {
            return Ok(await Mediator.Send(request));
        }
        [HttpDelete("{ganreId}")]
        public async Task<IActionResult> DeleteGanre([FromRoute]DeleteGanreCommand request)
        {
            return Ok(await Mediator.Send(request));
        }
    }
}

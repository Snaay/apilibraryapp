﻿using LibraryApp.Api.Common;
using LibraryApp.Application.Features.Bookings.Commands.GiveBookingBook;
using LibraryApp.Application.Features.Bookings.Commands.ReturnedBook;
using LibraryApp.Application.Features.Bookings.Queries.GetBookings;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LibraryApp.Api.Areas.Librarian.Controllers
{
    public class BookingController : LibrarianApiController
    {
        [HttpPost("search")] // api/booking/librarian/search
        public async Task<IActionResult> GetBookings(GetBookingsQuery request) // брони пользователей
        {
            return Ok(await Mediator.Send(request));
        }
        [HttpPost("give")] 
        public async Task<IActionResult> GiveBookingBook(GiveBookingBookCommand request) // выдать книгу
        {
            return Ok(await Mediator.Send(request));
        }
        [HttpPost("returned")]
        public async Task<IActionResult> ReturnedBookingBook(ReturnedBookingBookCommand request) // принес книгу
        {
            return Ok(await Mediator.Send(request));
        }
    }
}

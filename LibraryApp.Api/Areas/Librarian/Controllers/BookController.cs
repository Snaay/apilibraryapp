﻿using LibraryApp.Api.Common;
using LibraryApp.Application.Features.Books.Commands.CreateBook;
using LibraryApp.Application.Features.Books.Commands.DeleteBook;
using LibraryApp.Application.Features.Books.Commands.UpdateBook;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LibraryApp.Api.Areas.Librarian.Controllers
{
    public class BookController : LibrarianApiController
    {
        [HttpPost]
        public async Task<IActionResult> CreateBook([FromForm]CreateBookCommand request)
        {
            return Ok(await Mediator.Send(request));
        }
        [HttpPut]
        public async Task<IActionResult> UpdateBook([FromForm]UpdateBookCommand request)
        {
            return Ok(await Mediator.Send(request));
        }
        [HttpDelete("{bookId}")]
        public async Task<IActionResult> DeleteBook([FromRoute]DeleteBookCommand request)
        {
            return Ok(await Mediator.Send(request));
        }
    }
}

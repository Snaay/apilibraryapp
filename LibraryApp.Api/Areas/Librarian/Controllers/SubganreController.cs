﻿using LibraryApp.Api.Common;
using LibraryApp.Application.Features.Subganres.Commands.CreateSubganre;
using LibraryApp.Application.Features.Subganres.Commands.DeleteSubganre;
using LibraryApp.Application.Features.Subganres.Commands.UpdateSubganre;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LibraryApp.Api.Areas.Librarian.Controllers
{
    public class SubganreController : LibrarianApiController
    {
        [HttpPost]
        public async Task<IActionResult> CreateSubganre(CreateSubganreCommand request)
        {
            return Ok(await Mediator.Send(request));
        }
        [HttpPut]
        public async Task<IActionResult> UpdateSubganre(UpdateSubganreCommand request)
        {
            return Ok(await Mediator.Send(request));
        }
        [HttpDelete("{subganreId}")]
        public async Task<IActionResult> DeleteSubganres([FromRoute] DeleteSubganreCommand request)
        {
            return Ok(await Mediator.Send(request));
        }
    }
}

﻿using LibraryApp.Api.Common;
using LibraryApp.Application.Features.Publishers.Commands.CreatePublisher;
using LibraryApp.Application.Features.Publishers.Commands.DeletePublisher;
using LibraryApp.Application.Features.Publishers.Commands.UpdatePublisher;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LibraryApp.Api.Areas.Librarian.Controllers
{
    public class PublisherController : LibrarianApiController
    {
        [HttpPost]
        public async Task<IActionResult> CreatePublisher(CreatePublisherCommand request)
        {
            return Ok(await Mediator.Send(request));
        }
        [HttpPut]
        public async Task<IActionResult> UpdatePublisher(UpdatePublisherCommand request)
        {
            return Ok(await Mediator.Send(request));
        }
        [HttpDelete("{publisherId}")]
        public async Task<IActionResult> DeletePublisher([FromRoute]DeletePublisherCommand request)
        {
            return Ok(await Mediator.Send(request));
        }
    }
}

﻿using LibraryApp.Api.Common;
using LibraryApp.Application.Features.Comments.Commands.DeleteComment;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LibraryApp.Api.Areas.Librarian.Controllers
{
    public class CommentController : LibrarianApiController
    {
        [HttpDelete("{commentId}")]
        public async Task<IActionResult> CreateComment([FromRoute]DeleteCommentCommand request)
        {
            return Ok(await Mediator.Send(request));
        }
    }
}

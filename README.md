## Действия по запуску:
1. Изменить политику cors URL React приложения в файле "Startup.cs" в 63 строчке кода.
2. Настроить строку подключения в файле appsettings.json.
3. Применить миграцию: update-database (проект по умолчанию LibraryApp.Infrastructure)
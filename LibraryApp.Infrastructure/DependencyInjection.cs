﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Wrappers;
using LibraryApp.Domain.Entities.User;
using LibraryApp.Domain.Settings;
using LibraryApp.Infrastructure.Contexts;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Text;

namespace LibraryApp.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            // добавляются сервисы для EF Core
            services.AddDbContext<LibraryDbContext>(options => 
                options.UseNpgsql(configuration.GetConnectionString("LibraryDbConnection")));

            // указываем тип пользователя и тип роли, которые будут использоваться системой Identity
            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<LibraryDbContext>() // устанавливаем тип хранилища, которое будет применяться в Identity для хранения данных
                    .AddDefaultTokenProviders();

            services.AddScoped<ILibraryDbContext>(provider => provider.GetService<LibraryDbContext>());

            // преобразовывает секцию JwtSettings из appsettings.json в класс JwtSettings,
            // и с помощью DI передает в конструктор класса тип IOptions<JwtSettings>.
            // Далее переменная типа IOptions<JwtSettings> присваивается в поле JwtSettings
            services.Configure<JwtSettings>(configuration.GetSection("JwtSettings"));

            services.Configure<BookingSettings>(configuration.GetSection("BookingSettings"));

            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 6;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;

                options.User.AllowedUserNameCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";
                options.User.RequireUniqueEmail = true;
            });

            // конфигурация аутентификация по jwt
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultSignInScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, options => 
            {
                options.RequireHttpsMetadata = false; // в будущем установить в 'true'
                options.SaveToken = true;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true, // указываем, что будем проверять ключ которым подписывали токен JWT

                    ValidateIssuer = true, // укзывает будет ли валидироваться издатель при валидации токена
                    ValidIssuer = configuration["JwtSettings:Issuer"], // строка представляющая издателя

                    ValidateAudience = true, // будет ли валидироваться потребитель токена
                    ValidAudience = configuration["JwtSettings:Audience"], // установка потребителя токена

                    ValidateLifetime = true, // ставим true, так как хотим контролировать время жизни токена

                    ClockSkew = TimeSpan.Zero, // задаем погрешность при проверке времени жизни токена

                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JwtSettings:Key"])), // установка ключа безопасности
                };
                options.Events = new JwtBearerEvents
                {
                    OnAuthenticationFailed = context =>
                    {
                        context.NoResult();
                        context.Response.StatusCode = 401;
                        context.Response.ContentType = "application/json";

                        var serializerSettings = new JsonSerializerSettings();
                        serializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

                        var result = JsonConvert.SerializeObject(new Response<string>("Срок действия токена истек. "), serializerSettings);

                        return context.Response.WriteAsync(result);
                    },
                    OnChallenge = context =>
                    {
                        context.HandleResponse();
                        context.Response.StatusCode = 401;
                        context.Response.ContentType = "application/json";

                        var serializerSettings = new JsonSerializerSettings();
                        serializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

                        var result = JsonConvert.SerializeObject(new Response<string>("Вы не авторизованы"), serializerSettings);

                        return context.Response.WriteAsync(result);
                    },
                    OnForbidden = context =>
                    {
                        context.Response.StatusCode = 200;
                        context.Response.ContentType = "application/json";

                        var serializerSettings = new JsonSerializerSettings();
                        serializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

                        var result = JsonConvert.SerializeObject(new Response<string>("Нет прав доступа"), serializerSettings);

                        return context.Response.WriteAsync(result);
                    }
                };
            });
            return services;
        }
    }
}

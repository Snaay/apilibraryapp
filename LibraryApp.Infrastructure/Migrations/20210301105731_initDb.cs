﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace LibraryApp.Infrastructure.Migrations
{
    public partial class initDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Authors",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    FirstName = table.Column<string>(type: "text", nullable: true),
                    LastName = table.Column<string>(type: "text", nullable: true),
                    MiddleName = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Authors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Ganres",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ganres", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Publishers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Publishers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    Id = table.Column<string>(type: "text", nullable: false),
                    Name = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<string>(type: "text", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UserName = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    Email = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(type: "boolean", nullable: false),
                    PasswordHash = table.Column<string>(type: "text", nullable: true),
                    SecurityStamp = table.Column<string>(type: "text", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "text", nullable: true),
                    PhoneNumber = table.Column<string>(type: "text", nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(type: "boolean", nullable: false),
                    TwoFactorEnabled = table.Column<bool>(type: "boolean", nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    LockoutEnabled = table.Column<bool>(type: "boolean", nullable: false),
                    AccessFailedCount = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Subganres",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: true),
                    GanreId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subganres", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Subganres_Ganres_GanreId",
                        column: x => x.GanreId,
                        principalTable: "Ganres",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Books",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: true),
                    Description = table.Column<string>(type: "text", nullable: true),
                    PhotoPath = table.Column<string>(type: "text", nullable: true),
                    Year = table.Column<int>(type: "integer", nullable: false),
                    NumberPages = table.Column<int>(type: "integer", nullable: false),
                    Quantity = table.Column<int>(type: "integer", nullable: false),
                    DateAdded = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    PublisherId = table.Column<int>(type: "integer", nullable: false),
                    AuthorId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Books", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Books_Authors_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "Authors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Books_Publishers_PublisherId",
                        column: x => x.PublisherId,
                        principalTable: "Publishers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    RoleId = table.Column<string>(type: "text", nullable: false),
                    ClaimType = table.Column<string>(type: "text", nullable: true),
                    ClaimValue = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RoleClaims_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserId = table.Column<string>(type: "text", nullable: false),
                    ClaimType = table.Column<string>(type: "text", nullable: true),
                    ClaimValue = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserClaims_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(type: "text", nullable: false),
                    ProviderKey = table.Column<string>(type: "text", nullable: false),
                    ProviderDisplayName = table.Column<string>(type: "text", nullable: true),
                    UserId = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_UserLogins_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserProfiles",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    FirstName = table.Column<string>(type: "text", nullable: true),
                    LastName = table.Column<string>(type: "text", nullable: true),
                    MiddleName = table.Column<string>(type: "text", nullable: true),
                    PhotoPath = table.Column<string>(type: "text", nullable: true),
                    UserId = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserProfiles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserProfiles_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "text", nullable: false),
                    RoleId = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_UserRoles_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserRoles_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "text", nullable: false),
                    LoginProvider = table.Column<string>(type: "text", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    Value = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_UserTokens_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Bookings",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    BookId = table.Column<int>(type: "integer", nullable: false),
                    UserId = table.Column<string>(type: "text", nullable: true),
                    IsGiven = table.Column<bool>(type: "boolean", nullable: false),
                    BookingDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bookings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Bookings_Books_BookId",
                        column: x => x.BookId,
                        principalTable: "Books",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Bookings_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Comments",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Text = table.Column<string>(type: "text", nullable: true),
                    CommentDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    RatingTypeId = table.Column<string>(type: "text", nullable: false),
                    UserId = table.Column<string>(type: "text", nullable: true),
                    BookId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Comments_Books_BookId",
                        column: x => x.BookId,
                        principalTable: "Books",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Comments_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "GanreBooks",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    BookId = table.Column<int>(type: "integer", nullable: false),
                    SubganreId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GanreBooks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GanreBooks_Books_BookId",
                        column: x => x.BookId,
                        principalTable: "Books",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GanreBooks_Subganres_SubganreId",
                        column: x => x.SubganreId,
                        principalTable: "Subganres",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Notifications",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    BookId = table.Column<int>(type: "integer", nullable: false),
                    ArrivalDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Notifications", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Notifications_Books_BookId",
                        column: x => x.BookId,
                        principalTable: "Books",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SubNotifications",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    SubNotificationTypeId = table.Column<string>(type: "text", nullable: false),
                    BookId = table.Column<int>(type: "integer", nullable: false),
                    UserId = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubNotifications", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SubNotifications_Books_BookId",
                        column: x => x.BookId,
                        principalTable: "Books",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SubNotifications_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ReceiveNotifications",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    IsRead = table.Column<bool>(type: "boolean", nullable: false),
                    NotificationId = table.Column<int>(type: "integer", nullable: false),
                    UserId = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReceiveNotifications", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ReceiveNotifications_Notifications_NotificationId",
                        column: x => x.NotificationId,
                        principalTable: "Notifications",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ReceiveNotifications_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Authors",
                columns: new[] { "Id", "FirstName", "LastName", "MiddleName" },
                values: new object[,]
                {
                    { 1, "Йордон", "Эдвард", null },
                    { 2, "Джоан", "Роулинг", null },
                    { 3, "Сергей", "Абрамов", "Александрович" },
                    { 4, "Омраам", "Айванхов", "Микаэль" },
                    { 5, "Айзекс", "Мэхелия", null },
                    { 6, "Василий", "Аксенов", "Павлович" },
                    { 7, "Евгений", "Астахов", "Евгеньевич" },
                    { 8, "Анна", "Ахматова", "Андреевна" },
                    { 9, "Рудольф", "Баландин", "Константинович" },
                    { 10, "Михаил", "Барятинский", "Борисович" }
                });

            migrationBuilder.InsertData(
                table: "Ganres",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 6, "Разное" },
                    { 5, "Мистика/Ужасы" },
                    { 4, "Триллеры" },
                    { 3, "Детективы" },
                    { 2, "Фантастика" },
                    { 1, "Фэнтези" }
                });

            migrationBuilder.InsertData(
                table: "Publishers",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Эксмо" },
                    { 2, "Питер" },
                    { 3, "Вильямс" },
                    { 4, "Юрайт" },
                    { 5, "КноРус" },
                    { 6, "Добрая книга" },
                    { 7, "РИПОЛ" },
                    { 8, "Росмэн" },
                    { 9, "Азбука" },
                    { 10, "Лабиринт" }
                });

            migrationBuilder.InsertData(
                table: "Books",
                columns: new[] { "Id", "AuthorId", "DateAdded", "Description", "Name", "NumberPages", "PhotoPath", "PublisherId", "Quantity", "Year" },
                values: new object[,]
                {
                    { 15, 1, new DateTime(2021, 3, 1, 13, 57, 26, 599, DateTimeKind.Local).AddTicks(3605), "Гарри Поттеру предстоит обучения в Школе чародейства и волшебства «Хогвартс».", "Перси Джексон и последнее пророчество", 450, "Book/89272fd1762d4594bc5dda9caa8506a9.jpeg", 1, 1, 2019 },
                    { 1, 1, new DateTime(2021, 3, 1, 13, 57, 26, 599, DateTimeKind.Local).AddTicks(3144), "Гарри Поттеру предстоит обучения в Школе чародейства и волшебства «Хогвартс».", "Гарри Поттер и Кубок огня", 450, "Book/d875861422d542708d633e0c7b2269ab.jpeg", 1, 1, 2005 },
                    { 2, 1, new DateTime(2021, 3, 1, 13, 57, 26, 599, DateTimeKind.Local).AddTicks(3580), "Гарри Поттеру предстоит обучения в Школе чародейства и волшебства «Хогвартс».", "Гарри Поттер и Узник азкабана", 450, "Book/fb50648b5def437386985365db1ddf31.jpeg", 1, 1, 2006 },
                    { 4, 1, new DateTime(2021, 3, 1, 13, 57, 26, 599, DateTimeKind.Local).AddTicks(3587), "Гарри Поттеру предстоит обучения в Школе чародейства и волшебства «Хогвартс».", "Гарри Поттер и Тайная комната", 450, "Book/435ab1dd6b354e63aaafa436d13c3dec.jpeg", 1, 1, 2008 },
                    { 5, 1, new DateTime(2021, 3, 1, 13, 57, 26, 599, DateTimeKind.Local).AddTicks(3588), "Гарри Поттеру предстоит обучения в Школе чародейства и волшебства «Хогвартс».", "Гарри Поттер и Дары смерти", 450, "Book/65d877d428a24a23aaca57ced55d660c.jpeg", 1, 1, 2009 },
                    { 6, 1, new DateTime(2021, 3, 1, 13, 57, 26, 599, DateTimeKind.Local).AddTicks(3591), "Гарри Поттеру предстоит обучения в Школе чародейства и волшебства «Хогвартс».", "Гарри Поттер и Орден Феникса", 450, "Book/f4392ce4e5aa4dbaa5a68345ce5adaf6.jpeg", 1, 1, 2010 },
                    { 7, 1, new DateTime(2021, 3, 1, 13, 57, 26, 599, DateTimeKind.Local).AddTicks(3593), "Гарри Поттеру предстоит обучения в Школе чародейства и волшебства «Хогвартс».", "Гарри Поттер и Принц полукровка", 450, "Book/8471dc2e10064796adb332a428875ca4.jpeg", 1, 1, 2011 },
                    { 3, 1, new DateTime(2021, 3, 1, 13, 57, 26, 599, DateTimeKind.Local).AddTicks(3585), "Гарри Поттеру предстоит обучения в Школе чародейства и волшебства «Хогвартс».", "Гарри Поттер и Филосовский камень", 450, "Book/9f2f36a707a84afa9ed912deb44a57f4.jpeg", 1, 3, 2007 },
                    { 9, 1, new DateTime(2021, 3, 1, 13, 57, 26, 599, DateTimeKind.Local).AddTicks(3596), "Гарри Поттеру предстоит обучения в Школе чародейства и волшебства «Хогвартс».", "Властелин колец: Две крепости", 450, "Book/398c65d85c204b0987c47538584c767e.jpeg", 1, 3, 2013 },
                    { 10, 1, new DateTime(2021, 3, 1, 13, 57, 26, 599, DateTimeKind.Local).AddTicks(3597), "Гарри Поттеру предстоит обучения в Школе чародейства и волшебства «Хогвартс».", "Властелин колец: Возвращение короля", 450, "Book/72d6c0d8e77d48029dae06a64ab3856e.jpeg", 1, 3, 2014 },
                    { 11, 1, new DateTime(2021, 3, 1, 13, 57, 26, 599, DateTimeKind.Local).AddTicks(3599), "Гарри Поттеру предстоит обучения в Школе чародейства и волшебства «Хогвартс».", "Хоббит: пустошь смауга", 450, "Book/4491014d949a404a9bec0bdf5e94c884.jpeg", 1, 3, 2015 },
                    { 12, 1, new DateTime(2021, 3, 1, 13, 57, 26, 599, DateTimeKind.Local).AddTicks(3600), "Гарри Поттеру предстоит обучения в Школе чародейства и волшебства «Хогвартс».", "Хоббит: Нежданное путешествие", 450, "Book/12fa1bf575344460a8913188e0697996.jpeg", 1, 3, 2016 },
                    { 13, 1, new DateTime(2021, 3, 1, 13, 57, 26, 599, DateTimeKind.Local).AddTicks(3602), "Гарри Поттеру предстоит обучения в Школе чародейства и волшебства «Хогвартс».", "Хоббит: Битва пяти воинств", 450, "Book/b795d33f746f49f2ab8b78209345d3ed.jpeg", 1, 3, 2017 },
                    { 8, 1, new DateTime(2021, 3, 1, 13, 57, 26, 599, DateTimeKind.Local).AddTicks(3594), "Гарри Поттеру предстоит обучения в Школе чародейства и волшебства «Хогвартс».", "Властелин колец: Братство кольца", 450, "Book/d2aee34e384f43e78f504415174aacdf.jpeg", 1, 3, 2012 },
                    { 14, 1, new DateTime(2021, 3, 1, 13, 57, 26, 599, DateTimeKind.Local).AddTicks(3603), "Гарри Поттеру предстоит обучения в Школе чародейства и волшебства «Хогвартс».", "Приключения Шерлока Холмса", 450, "Book/c2ae834f3209476793e0a01b77b5ce61.jpeg", 1, 3, 2018 }
                });

            migrationBuilder.InsertData(
                table: "Subganres",
                columns: new[] { "Id", "GanreId", "Name" },
                values: new object[,]
                {
                    { 26, 6, "Детская литература" },
                    { 34, 6, "Юмор" },
                    { 33, 6, "Боевик" },
                    { 32, 6, "Приключенческий роман" },
                    { 31, 6, "Драма" },
                    { 30, 6, "Развитие личности" },
                    { 29, 6, "Бизнес-литература" },
                    { 28, 6, "Эзотерика" },
                    { 27, 6, "Неформат" },
                    { 25, 5, "Паранормальное" },
                    { 23, 4, "Мистический триллер" },
                    { 2, 1, "Историческое фэнтези" },
                    { 3, 1, "Городское фэнтези" },
                    { 4, 1, "Приключенческое фэнтези" },
                    { 5, 1, "Юмористическое фэнтези" },
                    { 6, 2, "Боевая фантастика" },
                    { 7, 2, "Научная фантастика" },
                    { 8, 2, "Космическая фантастика" },
                    { 9, 2, "Альтернативная история" },
                    { 10, 2, "Постапокалипсис" },
                    { 11, 2, "Антиутопия" },
                    { 12, 2, "Киберпанк" },
                    { 13, 2, "Юмористическая фантастика" },
                    { 14, 3, "Исторический детектив" },
                    { 15, 3, "Женский детектив" },
                    { 16, 3, "Криминальный детектив" },
                    { 17, 3, "Классический детектив" },
                    { 18, 3, "Полицейский детектив" },
                    { 19, 3, "Фантастический детектив" },
                    { 20, 3, "Магический детектив" },
                    { 21, 4, "Криминальный триллер" },
                    { 22, 4, "Политический триллер" },
                    { 24, 4, "Психологический триллер" },
                    { 1, 1, "Боевое фэнтези" }
                });

            migrationBuilder.InsertData(
                table: "GanreBooks",
                columns: new[] { "Id", "BookId", "SubganreId" },
                values: new object[,]
                {
                    { 1, 1, 1 },
                    { 25, 9, 1 },
                    { 26, 9, 3 },
                    { 27, 9, 4 },
                    { 28, 10, 1 },
                    { 29, 10, 3 },
                    { 30, 10, 4 },
                    { 31, 11, 1 },
                    { 32, 11, 4 },
                    { 33, 11, 5 },
                    { 34, 12, 1 },
                    { 35, 12, 4 },
                    { 36, 12, 5 },
                    { 37, 13, 1 },
                    { 38, 13, 4 },
                    { 39, 13, 5 },
                    { 40, 14, 16 },
                    { 41, 14, 17 },
                    { 42, 14, 18 },
                    { 43, 15, 4 },
                    { 24, 8, 4 },
                    { 44, 15, 6 },
                    { 23, 8, 3 },
                    { 21, 7, 4 },
                    { 2, 1, 2 },
                    { 3, 1, 4 },
                    { 4, 2, 1 },
                    { 5, 2, 2 },
                    { 6, 2, 4 },
                    { 7, 3, 1 },
                    { 8, 3, 2 },
                    { 9, 3, 4 },
                    { 10, 4, 1 },
                    { 11, 4, 2 },
                    { 12, 4, 4 },
                    { 13, 5, 1 },
                    { 14, 5, 2 },
                    { 15, 5, 4 },
                    { 16, 6, 1 },
                    { 17, 6, 2 },
                    { 18, 6, 4 },
                    { 19, 7, 1 },
                    { 20, 7, 2 },
                    { 22, 8, 1 },
                    { 45, 15, 13 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Bookings_BookId",
                table: "Bookings",
                column: "BookId");

            migrationBuilder.CreateIndex(
                name: "IX_Bookings_UserId",
                table: "Bookings",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Books_AuthorId",
                table: "Books",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_Books_PublisherId",
                table: "Books",
                column: "PublisherId");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_BookId",
                table: "Comments",
                column: "BookId");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_UserId",
                table: "Comments",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_GanreBooks_BookId",
                table: "GanreBooks",
                column: "BookId");

            migrationBuilder.CreateIndex(
                name: "IX_GanreBooks_SubganreId",
                table: "GanreBooks",
                column: "SubganreId");

            migrationBuilder.CreateIndex(
                name: "IX_Notifications_BookId",
                table: "Notifications",
                column: "BookId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ReceiveNotifications_NotificationId",
                table: "ReceiveNotifications",
                column: "NotificationId");

            migrationBuilder.CreateIndex(
                name: "IX_ReceiveNotifications_UserId",
                table: "ReceiveNotifications",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_RoleClaims_RoleId",
                table: "RoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "Roles",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Subganres_GanreId",
                table: "Subganres",
                column: "GanreId");

            migrationBuilder.CreateIndex(
                name: "IX_SubNotifications_BookId",
                table: "SubNotifications",
                column: "BookId");

            migrationBuilder.CreateIndex(
                name: "IX_SubNotifications_UserId",
                table: "SubNotifications",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserClaims_UserId",
                table: "UserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserLogins_UserId",
                table: "UserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserProfiles_UserId",
                table: "UserProfiles",
                column: "UserId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserRoles_RoleId",
                table: "UserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "Users",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "Users",
                column: "NormalizedUserName",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Bookings");

            migrationBuilder.DropTable(
                name: "Comments");

            migrationBuilder.DropTable(
                name: "GanreBooks");

            migrationBuilder.DropTable(
                name: "ReceiveNotifications");

            migrationBuilder.DropTable(
                name: "RoleClaims");

            migrationBuilder.DropTable(
                name: "SubNotifications");

            migrationBuilder.DropTable(
                name: "UserClaims");

            migrationBuilder.DropTable(
                name: "UserLogins");

            migrationBuilder.DropTable(
                name: "UserProfiles");

            migrationBuilder.DropTable(
                name: "UserRoles");

            migrationBuilder.DropTable(
                name: "UserTokens");

            migrationBuilder.DropTable(
                name: "Subganres");

            migrationBuilder.DropTable(
                name: "Notifications");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Ganres");

            migrationBuilder.DropTable(
                name: "Books");

            migrationBuilder.DropTable(
                name: "Authors");

            migrationBuilder.DropTable(
                name: "Publishers");
        }
    }
}

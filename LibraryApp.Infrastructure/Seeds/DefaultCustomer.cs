﻿using LibraryApp.Application.Enums;
using LibraryApp.Domain.Entities.User;
using Microsoft.AspNetCore.Identity;
using System;
using System.Threading.Tasks;

namespace LibraryApp.Infrastructure.Seeds
{
    public static class DefaultCustomer
    {
        public static async Task CreateDefaultCustomer(UserManager<ApplicationUser> userManager)
        {
            var customer = await userManager.FindByNameAsync("Snaay");

            userManager.PasswordValidators.Clear();

            if (customer == null)
            {
                customer = new ApplicationUser
                {
                    UserName = "Snaay",
                    Email = "snaay.snaay@mail.ru",
                    EmailConfirmed = true,
                    PhoneNumberConfirmed = true,
                    DateCreated = DateTime.Now,
                    UserProfile = new UserProfile
                    {
                        FirstName = null,
                        LastName = null,
                        MiddleName = null,
                        PhotoPath = null,
                    }
                };

                await userManager.CreateAsync(customer, "user123");

                await userManager.AddToRoleAsync(customer, Roles.Customer.ToString());
            }
        }
    }
}

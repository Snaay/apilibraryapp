﻿using LibraryApp.Application.Enums;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace LibraryApp.Infrastructure.Seeds
{
    public static class DefaultRoles
    {
        public static async Task CreateDefaultRolesAsync(RoleManager<IdentityRole> roleManager)
        {
            if (await roleManager.FindByNameAsync(Roles.Admin.ToString()) == null)
                await roleManager.CreateAsync(new IdentityRole(Roles.Admin.ToString()));

            if (await roleManager.FindByNameAsync(Roles.Librarian.ToString()) == null)
                await roleManager.CreateAsync(new IdentityRole(Roles.Librarian.ToString()));

            if (await roleManager.FindByNameAsync(Roles.Customer.ToString()) == null)
                await roleManager.CreateAsync(new IdentityRole(Roles.Customer.ToString()));
        }
    }
}

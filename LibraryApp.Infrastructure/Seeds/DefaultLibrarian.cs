﻿using LibraryApp.Application.Enums;
using LibraryApp.Domain.Entities.User;
using Microsoft.AspNetCore.Identity;
using System;
using System.Threading.Tasks;

namespace LibraryApp.Infrastructure.Seeds
{
    public static class DefaultLibrarian
    {
        public static async Task CreateDefaultLibrarian(UserManager<ApplicationUser> userManager)
        {
            var librarian = await userManager.FindByNameAsync("Librarian");

            userManager.PasswordValidators.Clear();

            if (librarian == null)
            {
                librarian = new ApplicationUser
                {
                    UserName = "Librarian",
                    Email = "librarian@mail.ru",
                    EmailConfirmed = true,
                    PhoneNumberConfirmed = true,
                    DateCreated = DateTime.Now,
                    UserProfile = new UserProfile
                    {
                        FirstName = null,
                        LastName = null,
                        MiddleName = null,
                        PhotoPath = null,
                    }
                };

                await userManager.CreateAsync(librarian, "lib123");

                await userManager.AddToRoleAsync(librarian, Roles.Librarian.ToString());
            }
        }
    }
}

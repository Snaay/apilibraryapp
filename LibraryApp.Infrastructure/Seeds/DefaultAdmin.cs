﻿using LibraryApp.Application.Enums;
using LibraryApp.Domain.Entities.User;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;

namespace LibraryApp.Infrastructure.Seeds
{
    public static class DefaultAdmin
    {
        public static async Task CreateDefaultAdmin(UserManager<ApplicationUser> userManager, IConfiguration configuration)
        {
            var admin = await userManager.FindByNameAsync("Admin");

            userManager.PasswordValidators.Clear();

            if (admin == null)
            {
                admin = new ApplicationUser
                {
                    UserName = "Admin",
                    Email = "mister.yamaletdinov@mail.ru",
                    EmailConfirmed = true,
                    PhoneNumberConfirmed = true,
                    DateCreated = DateTime.Now,
                    UserProfile = new UserProfile
                    {
                        FirstName = null,
                        LastName = null,
                        MiddleName = null,
                        PhotoPath = null,
                    }
                };

                await userManager.CreateAsync(admin, configuration["AdminSettings:Password"]);

                await userManager.AddToRolesAsync(admin, new string[] { Roles.Admin.ToString(), Roles.Librarian.ToString() });
            }
        }
    }
}

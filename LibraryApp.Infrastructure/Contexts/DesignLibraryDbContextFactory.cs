﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace LibraryApp.Infrastructure.Contexts
{
    public class DesignLibraryDbContextFactory : IDesignTimeDbContextFactory<LibraryDbContext> // для миграций
    {
        public LibraryDbContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile(@Directory.GetCurrentDirectory() + "/../LibraryApp.Api/appsettings.json")
                        .Build();

            var builder = new DbContextOptionsBuilder<LibraryDbContext>();
            var connectionString = configuration.GetConnectionString("LibraryDbConnection");
            builder.UseNpgsql(connectionString);
            return new LibraryDbContext(builder.Options);
        }
    }
}

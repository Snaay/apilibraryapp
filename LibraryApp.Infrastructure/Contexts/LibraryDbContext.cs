﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Domain.Entities;
using LibraryApp.Domain.Entities.Notification;
using LibraryApp.Domain.Entities.User;
using LibraryApp.Domain.Enums;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System;

namespace LibraryApp.Infrastructure.Contexts
{
    public class LibraryDbContext : IdentityDbContext<ApplicationUser>, ILibraryDbContext
    {
        public LibraryDbContext(DbContextOptions<LibraryDbContext> options) : base(options) { }
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Booking> Bookings { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<Publisher> Publishers { get; set; }
        public DbSet<Ganre> Ganres { get; set; }
        public DbSet<Subganre> Subganres { get; set; }
        public DbSet<GanreBook> GanreBooks { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<ReceiveNotification> ReceiveNotifications { get; set; }
        public DbSet<SubNotification> SubNotifications { get; set; }
        public DbSet<Comment> Comments { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.LogTo(Console.WriteLine);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            //переопределяем названия таблиц identity
            base.OnModelCreating(builder);
            builder.Entity<ApplicationUser>(entity => entity.ToTable(name: "Users"));
            builder.Entity<IdentityRole>(entity => entity.ToTable(name: "Roles"));
            builder.Entity<IdentityUserRole<string>>(entity => entity.ToTable("UserRoles"));
            builder.Entity<IdentityUserClaim<string>>(entity => entity.ToTable("UserClaims"));
            builder.Entity<IdentityUserLogin<string>>(entity => entity.ToTable("UserLogins"));
            builder.Entity<IdentityRoleClaim<string>>(entity => entity.ToTable("RoleClaims"));
            builder.Entity<IdentityUserToken<string>>(entity => entity.ToTable("UserTokens"));
            // EnumToNumberConverter будет хранит индекс, а не строку
            builder.Entity<SubNotification>()
                .Property(e => e.SubNotificationTypeId)
                    .HasConversion(new EnumToStringConverter<SubNotificationType>());
            builder.Entity<Comment>()
                .Property(e => e.RatingTypeId)
                    .HasConversion(new EnumToStringConverter<RatingType>());

            // при удалении пользователя, так же удалялись:
            builder.Entity<ApplicationUser>()
                .HasMany(a => a.ReceiveNotifications)
                .WithOne(b => b.User)
                .OnDelete(DeleteBehavior.Cascade);
            builder.Entity<ApplicationUser>()
                .HasOne(a => a.UserProfile)
                .WithOne(b => b.User)
                .OnDelete(DeleteBehavior.Cascade);
            builder.Entity<ApplicationUser>()
                .HasMany(a => a.SubNotifications)
                .WithOne(b => b.User)
                .OnDelete(DeleteBehavior.Cascade);
            // для удалении пользователя его комментарии остаются

            builder.Entity<Author>().HasData(new Author[]
            { 
                new Author { Id = 1, FirstName = "Йордон", LastName = "Эдвард", MiddleName = null },
                new Author { Id = 2, FirstName = "Джоан", LastName = "Роулинг", MiddleName = null },
                new Author { Id = 3, FirstName = "Сергей", LastName = "Абрамов", MiddleName = "Александрович" },
                new Author { Id = 4, FirstName = "Омраам", LastName = "Айванхов", MiddleName = "Микаэль" },
                new Author { Id = 5, FirstName = "Айзекс", LastName = "Мэхелия", MiddleName = null },
                new Author { Id = 6, FirstName = "Василий", LastName = "Аксенов", MiddleName = "Павлович" },
                new Author { Id = 7, FirstName = "Евгений", LastName = "Астахов", MiddleName = "Евгеньевич" },
                new Author { Id = 8, FirstName = "Анна", LastName = "Ахматова", MiddleName = "Андреевна" },
                new Author { Id = 9, FirstName = "Рудольф", LastName = "Баландин", MiddleName = "Константинович" },
                new Author { Id = 10, FirstName = "Михаил", LastName = "Барятинский", MiddleName = "Борисович" }
            });
            builder.Entity<Publisher>().HasData(new Publisher[]
            {
                new Publisher { Id = 1, Name = "Эксмо" },
                new Publisher { Id = 2, Name = "Питер" },
                new Publisher { Id = 3, Name = "Вильямс" },
                new Publisher { Id = 4, Name = "Юрайт" },
                new Publisher { Id = 5, Name = "КноРус" },
                new Publisher { Id = 6, Name = "Добрая книга" },
                new Publisher { Id = 7, Name = "РИПОЛ" },
                new Publisher { Id = 8, Name = "Росмэн" },
                new Publisher { Id = 9, Name = "Азбука" },
                new Publisher { Id = 10, Name = "Лабиринт" }

            });
            builder.Entity<Ganre>().HasData(new Ganre[]
            {
                new Ganre { Id = 1, Name = "Фэнтези" },
                new Ganre { Id = 2, Name = "Фантастика" },
                new Ganre { Id = 3, Name = "Детективы" },
                new Ganre { Id = 4, Name = "Триллеры" },
                new Ganre { Id = 5, Name = "Мистика/Ужасы" },
                new Ganre { Id = 6, Name = "Разное" },
            });
            builder.Entity<Subganre>().HasData(new Subganre[]
            {
                new Subganre { Id = 1, Name = "Боевое фэнтези", GanreId = 1 },
                new Subganre { Id = 2, Name = "Историческое фэнтези", GanreId = 1 },
                new Subganre { Id = 3, Name = "Городское фэнтези", GanreId = 1 },
                new Subganre { Id = 4, Name = "Приключенческое фэнтези", GanreId = 1 },
                new Subganre { Id = 5, Name = "Юмористическое фэнтези", GanreId = 1 },

                new Subganre { Id = 6, Name = "Боевая фантастика", GanreId = 2 },
                new Subganre { Id = 7, Name = "Научная фантастика", GanreId = 2 },
                new Subganre { Id = 8, Name = "Космическая фантастика", GanreId = 2 },
                new Subganre { Id = 9, Name = "Альтернативная история", GanreId = 2 },
                new Subganre { Id = 10, Name = "Постапокалипсис", GanreId = 2 },
                new Subganre { Id = 11, Name = "Антиутопия", GanreId = 2 },
                new Subganre { Id = 12, Name = "Киберпанк", GanreId = 2 },
                new Subganre { Id = 13, Name = "Юмористическая фантастика", GanreId = 2 },

                new Subganre { Id = 14, Name = "Исторический детектив", GanreId = 3 },
                new Subganre { Id = 15, Name = "Женский детектив", GanreId = 3 },
                new Subganre { Id = 16, Name = "Криминальный детектив", GanreId = 3 },
                new Subganre { Id = 17, Name = "Классический детектив", GanreId = 3 },
                new Subganre { Id = 18, Name = "Полицейский детектив", GanreId = 3 },
                new Subganre { Id = 19, Name = "Фантастический детектив", GanreId = 3 },
                new Subganre { Id = 20, Name = "Магический детектив", GanreId = 3 },

                new Subganre { Id = 21, Name = "Криминальный триллер", GanreId = 4 },
                new Subganre { Id = 22, Name = "Политический триллер", GanreId = 4 },
                new Subganre { Id = 23, Name = "Мистический триллер", GanreId = 4 },
                new Subganre { Id = 24, Name = "Психологический триллер", GanreId = 4 },

                new Subganre { Id = 25, Name = "Паранормальное", GanreId = 5 },

                new Subganre { Id = 26, Name = "Детская литература", GanreId = 6 },
                new Subganre { Id = 27, Name = "Неформат", GanreId = 6 },
                new Subganre { Id = 28, Name = "Эзотерика", GanreId = 6 },
                new Subganre { Id = 29, Name = "Бизнес-литература", GanreId = 6 },
                new Subganre { Id = 30, Name = "Развитие личности", GanreId = 6 },
                new Subganre { Id = 31, Name = "Драма", GanreId = 6 },
                new Subganre { Id = 32, Name = "Приключенческий роман", GanreId = 6 },
                new Subganre { Id = 33, Name = "Боевик", GanreId = 6 },
                new Subganre { Id = 34, Name = "Юмор", GanreId = 6 },
            });
            builder.Entity<Book>().HasData(new Book[]
            {
                new Book
                {
                    Id = 1,
                    Name = "Гарри Поттер и Кубок огня",
                    Description = "Гарри Поттеру предстоит обучения в Школе чародейства и волшебства «Хогвартс».",
                    PhotoPath = "Book/d875861422d542708d633e0c7b2269ab.jpeg",
                    Year = 2005,
                    NumberPages = 450,
                    Quantity = 1,
                    AuthorId = 1,
                    PublisherId = 1,
                    DateAdded = DateTime.Now
                },
                new Book
                {
                    Id = 2,
                    Name = "Гарри Поттер и Узник азкабана",
                    Description = "Гарри Поттеру предстоит обучения в Школе чародейства и волшебства «Хогвартс».",
                    PhotoPath = "Book/fb50648b5def437386985365db1ddf31.jpeg",
                    Year = 2006,
                    NumberPages = 450,
                    Quantity = 1,
                    AuthorId = 1,
                    PublisherId = 1,
                    DateAdded = DateTime.Now
                },
                new Book
                {
                    Id = 3,
                    Name = "Гарри Поттер и Филосовский камень",
                    Description = "Гарри Поттеру предстоит обучения в Школе чародейства и волшебства «Хогвартс».",
                    PhotoPath = "Book/9f2f36a707a84afa9ed912deb44a57f4.jpeg",
                    Year = 2007,
                    NumberPages = 450,
                    Quantity = 1,
                    AuthorId = 1,
                    PublisherId = 1,
                    DateAdded = DateTime.Now
                },
                new Book
                {
                    Id = 4,
                    Name = "Гарри Поттер и Тайная комната",
                    Description = "Гарри Поттеру предстоит обучения в Школе чародейства и волшебства «Хогвартс».",
                    PhotoPath = "Book/435ab1dd6b354e63aaafa436d13c3dec.jpeg",
                    Year = 2008,
                    NumberPages = 450,
                    Quantity = 1,
                    AuthorId = 1,
                    PublisherId = 1,
                    DateAdded = DateTime.Now
                },
                new Book
                {
                    Id = 5,
                    Name = "Гарри Поттер и Дары смерти",
                    Description = "Гарри Поттеру предстоит обучения в Школе чародейства и волшебства «Хогвартс».",
                    PhotoPath = "Book/65d877d428a24a23aaca57ced55d660c.jpeg",
                    Year = 2009,
                    NumberPages = 450,
                    Quantity = 1,
                    AuthorId = 1,
                    PublisherId = 1,
                    DateAdded = DateTime.Now
                },
                new Book
                {
                    Id = 6,
                    Name = "Гарри Поттер и Орден Феникса",
                    Description = "Гарри Поттеру предстоит обучения в Школе чародейства и волшебства «Хогвартс».",
                    PhotoPath = "Book/f4392ce4e5aa4dbaa5a68345ce5adaf6.jpeg",
                    Year = 2010,
                    NumberPages = 450,
                    Quantity = 1,
                    AuthorId = 1,
                    PublisherId = 1,
                    DateAdded = DateTime.Now
                },
                new Book
                {
                    Id = 7,
                    Name = "Гарри Поттер и Принц полукровка",
                    Description = "Гарри Поттеру предстоит обучения в Школе чародейства и волшебства «Хогвартс».",
                    PhotoPath = "Book/8471dc2e10064796adb332a428875ca4.jpeg",
                    Year = 2011,
                    NumberPages = 450,
                    Quantity = 1,
                    AuthorId = 1,
                    PublisherId = 1,
                    DateAdded = DateTime.Now
                },
                new Book
                {
                    Id = 8,
                    Name = "Властелин колец: Братство кольца",
                    Description = "Гарри Поттеру предстоит обучения в Школе чародейства и волшебства «Хогвартс».",
                    PhotoPath = "Book/d2aee34e384f43e78f504415174aacdf.jpeg",
                    Year = 2012,
                    NumberPages = 450,
                    Quantity = 1,
                    AuthorId = 1,
                    PublisherId = 1,
                    DateAdded = DateTime.Now
                },
                new Book
                {
                    Id = 9,
                    Name = "Властелин колец: Две крепости",
                    Description = "Гарри Поттеру предстоит обучения в Школе чародейства и волшебства «Хогвартс».",
                    PhotoPath = "Book/398c65d85c204b0987c47538584c767e.jpeg",
                    Year = 2013,
                    NumberPages = 450,
                    Quantity = 3,
                    AuthorId = 1,
                    PublisherId = 1,
                    DateAdded = DateTime.Now
                },
                new Book
                {
                    Id = 10,
                    Name = "Властелин колец: Возвращение короля",
                    Description = "Гарри Поттеру предстоит обучения в Школе чародейства и волшебства «Хогвартс».",
                    PhotoPath = "Book/72d6c0d8e77d48029dae06a64ab3856e.jpeg",
                    Year = 2014,
                    NumberPages = 450,
                    Quantity = 3,
                    AuthorId = 1,
                    PublisherId = 1,
                    DateAdded = DateTime.Now
                },
                new Book
                {
                    Id = 11,
                    Name = "Хоббит: пустошь смауга",
                    Description = "Гарри Поттеру предстоит обучения в Школе чародейства и волшебства «Хогвартс».",
                    PhotoPath = "Book/4491014d949a404a9bec0bdf5e94c884.jpeg",
                    Year = 2015,
                    NumberPages = 450,
                    Quantity = 3,
                    AuthorId = 1,
                    PublisherId = 1,
                    DateAdded = DateTime.Now
                },
                new Book
                {
                    Id = 12,
                    Name = "Хоббит: Нежданное путешествие",
                    Description = "Гарри Поттеру предстоит обучения в Школе чародейства и волшебства «Хогвартс».",
                    PhotoPath = "Book/12fa1bf575344460a8913188e0697996.jpeg",
                    Year = 2016,
                    NumberPages = 450,
                    Quantity = 3,
                    AuthorId = 1,
                    PublisherId = 1,
                    DateAdded = DateTime.Now
                },
                new Book
                {
                    Id = 13,
                    Name = "Хоббит: Битва пяти воинств",
                    Description = "Гарри Поттеру предстоит обучения в Школе чародейства и волшебства «Хогвартс».",
                    PhotoPath = "Book/b795d33f746f49f2ab8b78209345d3ed.jpeg",
                    Year = 2017,
                    NumberPages = 450,
                    Quantity = 3,
                    AuthorId = 1,
                    PublisherId = 1,
                    DateAdded = DateTime.Now
                },
                new Book
                {
                    Id = 14,
                    Name = "Приключения Шерлока Холмса",
                    Description = "Гарри Поттеру предстоит обучения в Школе чародейства и волшебства «Хогвартс».",
                    PhotoPath = "Book/c2ae834f3209476793e0a01b77b5ce61.jpeg",
                    Year = 2018,
                    NumberPages = 450,
                    Quantity = 3,
                    AuthorId = 1,
                    PublisherId = 1,
                    DateAdded = DateTime.Now
                },
                new Book
                {
                    Id = 15,
                    Name = "Перси Джексон и последнее пророчество",
                    Description = "Гарри Поттеру предстоит обучения в Школе чародейства и волшебства «Хогвартс».",
                    PhotoPath = "Book/89272fd1762d4594bc5dda9caa8506a9.jpeg",
                    Year = 2019,
                    NumberPages = 450,
                    Quantity = 3,
                    AuthorId = 1,
                    PublisherId = 1,
                    DateAdded = DateTime.Now
                },
            });
            builder.Entity<GanreBook>().HasData(new GanreBook[]
            {
                new GanreBook { Id = 1, BookId = 1, SubganreId = 1 },
                new GanreBook { Id = 2, BookId = 1, SubganreId = 2 },
                new GanreBook { Id = 3, BookId = 1, SubganreId = 4 },

                new GanreBook { Id = 4, BookId = 2, SubganreId = 1 },
                new GanreBook { Id = 5, BookId = 2, SubganreId = 2 },
                new GanreBook { Id = 6, BookId = 2, SubganreId = 4 },

                new GanreBook { Id = 7, BookId = 3, SubganreId = 1 },
                new GanreBook { Id = 8, BookId = 3, SubganreId = 2 },
                new GanreBook { Id = 9, BookId = 3, SubganreId = 4 },

                new GanreBook { Id = 10, BookId = 4, SubganreId = 1 },
                new GanreBook { Id = 11, BookId = 4, SubganreId = 2 },
                new GanreBook { Id = 12, BookId = 4, SubganreId = 4 },

                new GanreBook { Id = 13, BookId = 5, SubganreId = 1 },
                new GanreBook { Id = 14, BookId = 5, SubganreId = 2 },
                new GanreBook { Id = 15, BookId = 5, SubganreId = 4 },

                new GanreBook { Id = 16, BookId = 6, SubganreId = 1 },
                new GanreBook { Id = 17, BookId = 6, SubganreId = 2 },
                new GanreBook { Id = 18, BookId = 6, SubganreId = 4 },

                new GanreBook { Id = 19, BookId = 7, SubganreId = 1 },
                new GanreBook { Id = 20, BookId = 7, SubganreId = 2 },
                new GanreBook { Id = 21, BookId = 7, SubganreId = 4 },

                new GanreBook { Id = 22, BookId = 8, SubganreId = 1 },
                new GanreBook { Id = 23, BookId = 8, SubganreId = 3 },
                new GanreBook { Id = 24, BookId = 8, SubganreId = 4 },

                new GanreBook { Id = 25, BookId = 9, SubganreId = 1 },
                new GanreBook { Id = 26, BookId = 9, SubganreId = 3 },
                new GanreBook { Id = 27, BookId = 9, SubganreId = 4 },

                new GanreBook { Id = 28, BookId = 10, SubganreId = 1 },
                new GanreBook { Id = 29, BookId = 10, SubganreId = 3 },
                new GanreBook { Id = 30, BookId = 10, SubganreId = 4 },

                new GanreBook { Id = 31, BookId = 11, SubganreId = 1 },
                new GanreBook { Id = 32, BookId = 11, SubganreId = 4 },
                new GanreBook { Id = 33, BookId = 11, SubganreId = 5 },

                new GanreBook { Id = 34, BookId = 12, SubganreId = 1 },
                new GanreBook { Id = 35, BookId = 12, SubganreId = 4 },
                new GanreBook { Id = 36, BookId = 12, SubganreId = 5 },

                new GanreBook { Id = 37, BookId = 13, SubganreId = 1 },
                new GanreBook { Id = 38, BookId = 13, SubganreId = 4 },
                new GanreBook { Id = 39, BookId = 13, SubganreId = 5 },

                new GanreBook { Id = 40, BookId = 14, SubganreId = 16 },
                new GanreBook { Id = 41, BookId = 14, SubganreId = 17 },
                new GanreBook { Id = 42, BookId = 14, SubganreId = 18 },

                new GanreBook { Id = 43, BookId = 15, SubganreId = 4 },
                new GanreBook { Id = 44, BookId = 15, SubganreId = 6 },
                new GanreBook { Id = 45, BookId = 15, SubganreId = 13 },
            });
        }
    }
}

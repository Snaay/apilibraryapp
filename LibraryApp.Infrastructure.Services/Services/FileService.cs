﻿using LibraryApp.Application.Common.Interfaces.File;
using LibraryApp.Application.DTO.File;
using Microsoft.AspNetCore.Hosting;
using System;
using System.IO;
using System.Threading.Tasks;

namespace LibraryApp.Infrastructure.Services.Services
{
    public class FileService : IFileService
    {
        private readonly string _webRootPath;
        public FileService(IWebHostEnvironment env)
        {
            _webRootPath = env.WebRootPath;
        }
        public async Task<string> CreateFile(CreateFileRequest request)
        {
            string folderName = request.FolderTypeId.ToString();

            string fileName = Guid.NewGuid().ToString().Replace("-", string.Empty) + ".jpeg";

            string relativePath = string.Format("{0}/{1}", folderName, fileName);

            string fullPath = string.Format("{0}/{1}/{2}", _webRootPath, folderName, fileName);

            string createFolderPath = string.Format("{0}/{1}", _webRootPath, folderName);

            Directory.CreateDirectory(createFolderPath);

            using (var fileStream = new FileStream(fullPath, FileMode.Create))
            {
                await request.FileStream.CopyToAsync(fileStream);
            }

            return relativePath;
        }
        public async Task<string> UpdateFile(UpdateFileRequest request)
        {
            string fullPath = string.Format("{0}/{1}", _webRootPath, request.FilePath);

            using (var fileStream = new FileStream(fullPath, FileMode.Create))
            {
                await request.FileStream.CopyToAsync(fileStream);
            }

            return request.FilePath;
        }
        public async Task DeleteFile(DeleteFileRequest request)
        {
            string fullPath = string.Format("{0}/{1}", _webRootPath, request.FilePath);

            using (FileStream stream = new FileStream(fullPath, FileMode.Open, FileAccess.Read, FileShare.None, 1, FileOptions.DeleteOnClose | FileOptions.Asynchronous))
            {
                await stream.FlushAsync();
            }

            await Task.CompletedTask;
        }
    }
}

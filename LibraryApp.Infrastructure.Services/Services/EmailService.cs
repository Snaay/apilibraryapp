﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.DTO.Email;
using LibraryApp.Domain.Settings;
using MailKit.Security;
using Microsoft.Extensions.Options;
using MimeKit;
using MailKit.Net.Smtp;
using System.Threading.Tasks;
using LibraryApp.Application.Exceptions;

namespace LibraryApp.Infrastructure.Services.Services
{
    public class EmailService : IEmailService
    {
        public MailSettings _mailSettings { get; }

        public EmailService(IOptions<MailSettings> mailSettings) => _mailSettings = mailSettings.Value;

        public async Task SendAsync(EmailRequest request)
        {
            try
            {
                var email = new MimeMessage();

                email.Sender = MailboxAddress.Parse(request.From ?? _mailSettings.EmailFrom);
                email.To.Add(MailboxAddress.Parse(request.To));
                email.Subject = request.Subject;

                var builder = new BodyBuilder();

                builder.HtmlBody = request.Body;
                email.Body = builder.ToMessageBody();

                using (var smtp = new SmtpClient())
                {
                    await smtp.ConnectAsync(_mailSettings.SmtpHost, _mailSettings.SmtpPort, SecureSocketOptions.StartTls);
                    await smtp.AuthenticateAsync(_mailSettings.SmtpUser, _mailSettings.SmtpPass);
                    await smtp.SendAsync(email);

                    await smtp.DisconnectAsync(true);
                }

            }
            catch
            {
                throw new ApiException("Ошибка при отправке почты");
            }
        }
    }
}

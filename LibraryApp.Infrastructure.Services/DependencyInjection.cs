﻿using LibraryApp.Application.Common.Interfaces;
using LibraryApp.Application.Common.Interfaces.File;
using LibraryApp.Domain.Settings;
using LibraryApp.Infrastructure.Services.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace LibraryApp.Infrastructure.Services
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<MailSettings>(configuration.GetSection("MailSettings"));

            services.AddTransient<IEmailService, EmailService>();

            services.AddTransient<IFileService, FileService>();

            return services;
        }
    }
}
